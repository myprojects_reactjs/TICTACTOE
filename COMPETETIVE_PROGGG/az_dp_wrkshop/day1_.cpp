#include<bits/stdc++.h>
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

using namespace std;
int count1=0;

vector<int>queens(8,-1);
bool checkr(pair<int,int>pos)
{

    for(int i=0;i<pos.first;i++)
    {
        if(pos.second==queens[i]||(abs(pos.second-queens[i])==abs(pos.first-i)))
            return false;
    }

    return true;

}

bool solve(vector<int>*blk)
{
static int i=0;
/*
........
........
..*.....
........
........
.....**.
...*....
........
*/
if(i==8)
{
    
    count1++;
    //cout<<count1<<'\n';
    return true;
}
for(int j=0;j<8;j++)
{
    if(blk[i][j]==1)
        continue;

    pair<int,int>temp={i,j};

    if(checkr(temp)==true)
    {   
        //cout<<i<<":"<<j<<'\n';
        queens[i]=j;
        i++;
        solve(blk);
        i--;
        queens[i]=-1;
    }
}
return false;

}


int main()
{
    IOS
    string s;
    vector<string>vec;
    for(int i=0;i<8;i++)
    {
        getline(cin,s);
        vec.push_back(s);
       
    }
   
    //vector<vector<int>>blk;
    vector<int>blk[8];
   
    for(int i=0;i<8;i++)
    {   
        blk[i]=vector<int>(8,0);
        cout<<i<<":"<<vec[i]<<'\n';
         
    }
   
    for(int i=0;i<8;i++)
    {
         for(int j=0;j<8;j++)
         {
            if(vec[i][j]=='*')
              {  blk[i][j]=1;
                
              }
         }
         
    }
    
    solve(blk);
    
    cout<<count1;

    //return 0;
}