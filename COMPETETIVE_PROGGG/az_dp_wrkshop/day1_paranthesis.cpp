#include<bits/stdc++.h>
using namespace std;
void solve(string s,int i,int j)
{
    string temp;
    if(i==0&&j==0)
    {
        
        cout<<s<<'\n';
        return;
    }
    if(i==j)
    {
        s+='(';
        solve(s,i-1,j);
    }
    else if(i!=0&&i<j)
    {
        temp=s+'(';
        solve(temp,i-1,j);
        temp=s+')';
        solve(temp,i,j-1);
    }
    else
    {
        temp=s+')';
        solve(temp,i,j-1);
    }
}
int main()
{
    int n;
    cin>>n;
    string s;
    solve(s,n,n);
    return 0;
}