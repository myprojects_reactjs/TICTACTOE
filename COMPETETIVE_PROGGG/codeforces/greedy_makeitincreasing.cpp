#include<iostream>
#include<vector>
#include<queue>
#define ll long long
using namespace std;
ll arr[5002];
int main(){
    int n;
    cin>>n;
    for(int i=0;i<n;i++)
    {
        cin>>arr[i];
    }
    
    vector<ll>answ;
    for(int i=0;i<n;i++)
    {
        //left summation
        ll ans=0;
        ll prev=0,temp1=0,temp2;
        for(int j=i-1;j>-1;j--)
        {
            if(arr[j]>prev)
            {
                ans++;
                prev=arr[j];
            }
            else if(arr[j]<=prev)
            {
                temp1=(prev+1);
                temp2=temp1/arr[j] +(temp1%arr[j]>0?1:0);
                ans+=temp2;
                prev=arr[j]*temp2;
            }
        }
        prev=0;temp1=0;temp2=0;
        for(int j=i+1;j<n;j++)
        {
            if(arr[j]>prev)
            {
                ans++;
                prev=arr[j];
            }
            else if(arr[j]<=prev)
            {
                temp1=(prev+1);
                temp2=temp1/arr[j] +(temp1%arr[j]>0?1:0);
                ans+=temp2;
                prev=arr[j]*temp2;
            }
        }
        answ.push_back(ans);
    
        //cout<<ans<<" ";
    }
    priority_queue<ll,vector<ll>,greater<ll>>pq(answ.begin(),answ.end());
    cout<<pq.top();
    return 0;
}