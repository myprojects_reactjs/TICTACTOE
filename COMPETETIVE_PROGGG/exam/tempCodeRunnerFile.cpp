#include <bits/stdc++.h>
#define ll long long
using namespace std;

#define IOS                  \
    ios::sync_with_stdio(0); \
    cin.tie(0);              \
    cout.tie(0);

 
void SieveOfEratosthenes(bool *prime,map<int,int>&mp)
{
   
   
    for (int p = 2; p * p <= 1000000; p++) {
    
        if (prime[p] == true) {
           
            for (int i = p * p; i <= 1000000; i += p)
            {
                prime[i] = false;
                if(i%2==1&&i==p*p)
                    mp[i]=1;
            }
        }
    }
}
int binaryToDecimal(string n)
{
    string num = n;
    int dec_value = 0;
 
    // Initializing base value to 1, i.e 2^0
    int base = 1;
 
    int len = num.length();
    for (int i = len - 1; i >= 0; i--) {
        if (num[i] == '1')
            dec_value += base;
        base = base * 2;
    }
 
    return dec_value;
}
int main()
{
   int n,t,a;
   cin>>t;
   bool prime[1000001];
   memset(prime, true, sizeof(prime));
   map<int,int>mp;
   SieveOfEratosthenes(prime,mp);
   
   string ans="";
   int count=0;
   cin>>n;
   while(t--)
   {
       
       int win=0;
       vector<int>even,odd,bodd,primes1;
       for(int i=1;i<=n;i++)
       {
           cin>>a;
           if(prime[a]==true)
           primes1.push_back(a);
           else
           {
               if(a%2==0)
               even.push_back(a);
               else
               {
                   if(mp[a]==1)
                   bodd.push_back(a);
                   else
                   odd.push_back(a);

               }
           }
       }
       if(even.size()%2!=0)
        win=1;
        if(odd.size()%2!=0)
        win=1;
        if(!bodd.empty())
        {
            if(bodd.size()%2==0)
            {
                win=0;
            }
        }
        ans+=win+'0';
   }
   reverse(ans.begin(), ans.end()); 
   cout<<binaryToDecimal(ans);
    return 0;
}