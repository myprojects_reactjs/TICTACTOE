#include<iostream>
#include<queue>
#include<vector>
#include<unordered_map>
#include<map>
#include<algorithm>
#define ll long long
using namespace std;
struct cmp
{
bool operator ()(pair<ll,ll>a,pair<ll,ll>b)
{
    if(b.second > a.second)
    return false;
    if(b.second==a.second&&b.first<a.first)
    return false;

    return true;
}
};
/*int n;
int ans,ans1;
struct task{
	int c,b;
}a[100000005];
bool cmp(task x,task y) {
  	if(x.c==y.c)
	return x.b<y.b;
	return x.c<y.c;
}
int main(){
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>a[i].b>>a[i].c;
	}
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++){
		ans+=a[i].b;
		if(ans>a[i].c){
			cout<<"No"<<endl;
			return 0;
		}
	}
	cout<<"Yes"<<endl;
	return 0;
} */
int main()
{
    ll n;
    ll a,b;
    cin>>n;
    vector<pair<ll,ll>>jobs;
    for(int i=0;i<n;i++)
    {
        cin>>a>>b;
        jobs.push_back({a,b});
    } 
    sort(jobs.begin(),jobs.end(),cmp());
    unordered_map<ll,ll>mp;
//start time is 0, end time is max deadline
    mp[0]=-1;
    ll start,end,flag=0;
    for(int i=0;i<n;i++)
    {
        pair<ll,ll>temp=jobs[i];
        end=temp.second;
        while(end!=0)
        {
            if(end-temp.first<=0)
            {
                end=0;
                break;
            }
            if(mp[end]==0)
            {
                mp[end]=end-temp.first;                
                break;
            }
            end=mp[end];
        }
        if(end==0)
        {
            cout<<"No";
            flag=1;
            break;
        }


    }
    if(flag==0)
    cout<<"Yes";
    return 0;
}