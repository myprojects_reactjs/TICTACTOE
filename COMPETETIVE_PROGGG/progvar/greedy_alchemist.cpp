#include<iostream>
#include<queue>
#include <iomanip>
#define ll long long
using namespace std;
int main()
{
    vector<double>input;
    int n;
    double a,b,c;
    cin>>n;
    for(int i=0;i<n;i++)
    {
        cin>>a;
        input.push_back(a);
    }
    priority_queue<double,vector<double>,greater<double>>pq(input.begin(),input.end());
    for(int i=1;i<=n-1;i++)
    {
        a=pq.top();
        pq.pop();
        b=pq.top();
        pq.pop();
        c=(a+b)/2.0;
        pq.push(c);
    }
    cout<<fixed<<setprecision(1)<<pq.top();
    return 0;
}