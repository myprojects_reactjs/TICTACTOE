#include<iostream>
#include<queue>
#include<algorithm>
#include<set>
#include<unordered_map>
#define ll long long
using namespace std;
/*approach:
thinking to take map with pq as value,take each max from respective keys once and then take max of from next time
*/int main()
{
    ll n,k,a,b;
    cin>>n>>k;
    vector<pair<ll,ll>>vall_pairs;
    vector<ll>max_delicious(100006,0);
    for(int i=1;i<=n;i++)
    {
        cin>>a>>b;
        vall_pairs.push_back({b,a});
        if(max_delicious[a]<b)
         max_delicious[a]=b;
    }
    vector<pair<ll,ll>>max_del;
    for(ll i=1;i<=n;i++)
    {
        if(max_delicious[i]>0)
        {
            max_del.push_back({max_delicious[i],i});
        }
    }


    priority_queue<pair<ll,ll>,vector<pair<ll,ll>>>pall_pairs(vall_pairs.begin(),vall_pairs.end());
    priority_queue<pair<ll,ll>,vector<pair<ll,ll>>>max_pair(max_del.begin(),max_del.end()); //{b,a}->
    //deliciousness,topin no
    pair<ll,ll>pall,max;
    unordered_map<ll,ll>s;
    ll total_cost=0,variety=1;

    total_cost+=pall_pairs.top().first;
    s[pall_pairs.top().second]=0;
    pall_pairs.pop();max_pair.pop();
    int i;
    for(i=1;i<=k-1;)
    {
        if(!max_pair.empty())
        {
            pall=pall_pairs.top();max=max_pair.top();
            if(max.second==pall.second)
            {
                total_cost+=max.first;
                pall_pairs.pop();max_pair.pop();
                variety++;
            }
            else
            {
                if(s[pall.second]==1)
                {
                    s[pall.second]=0;
                    pall_pairs.pop();
                    continue;
                }
                else
                {
                    ll temp=variety+1;
                    //cout<<temp*temp+max.first<<" "<<pall.first+variety*variety<<"\n";
                    if(temp*temp+max.first>=pall.first+(variety==1?0:variety*variety))
                    {
                        variety++;
                        total_cost+=max.first;
                        s[max.second]=1;
                        max_pair.pop();
                    }
                    else
                    {
                        total_cost+=pall.first;
                        //cout<<"pall "<<pall.first<<" ";
                        pall_pairs.pop();
                    }
                }
            }
            i++;
        }
        else
        {
            while(i<k)
            {
                total_cost+=pall_pairs.top().first;
                i++;
                pall_pairs.pop();
            }
        }

    }
   
    total_cost=total_cost+(variety==1?0:variety*variety);
    cout<<total_cost;
    

    
    
    return 0;
}