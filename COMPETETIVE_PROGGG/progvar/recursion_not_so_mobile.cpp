#include<bits/stdc++.h>
using namespace std;
struct node
{
int wl,wr,dl,dr;
node* ln;
node* rn;

node(int wl=0,int wr=0,int dl=0,int dr=0,node *ln=nullptr,node*rn=nullptr):wl{wl},wr{wr},dl{dl},dr{dr},ln{nullptr},rn{nullptr}{}
};
node* head=nullptr;
node* temp=nullptr;

void insert(node*& root,node*& temp1)
{
    if(root==nullptr)
    {
        
        root=temp1;
        return;
    }

    if(root!=nullptr&&root->wl==0&&root->ln==nullptr)
    {
        
        root->ln=temp1;
        temp1=nullptr;
        return;
    }
    if(temp1!=nullptr&&root->wl==0&&root->ln!=nullptr&&(root->ln->wl==0||root->ln->wr==0))
    {
        insert(root->ln,temp1);
    }
    if(root->wr==0&&root->rn==nullptr)
    {
        root->rn=temp1;
        temp1=nullptr;
        return;
    }
    if(temp1!=nullptr&&root->wr==0&&root->rn!=nullptr&&(root->rn->wl==0||root->rn->wr==0))
    {
        insert(root->rn,temp1);
    }
   
}

pair<bool,int> traverse(node*& root)
{
     
    pair<bool,int>left,right;
    if(root->ln==nullptr&&root->rn==nullptr)
    {
        if(root->wl*root->dl==root->wr*root->dr)
        {
            return {true,root->wl+root->wr};
        }
       
    }

     if(root->ln!=nullptr)
    {
        left=traverse(root->ln);
        if(left.first==true)
         root->wl=left.second;
    }

    if(root->rn!=nullptr) 
    {
        right=traverse(root->rn);
        if(right.first==true)
         root->wr=right.second;
        

    }

    if(root->wl*root->dl==root->wr*root->dr)
    {
        return {true,root->wl+root->wr};
    }
    
    return {false,-1};
    
}



int main()
{
    int t;
    cin>>t;
    int wl,wr,dl,dr;
    string s;
    getline(cin,s);
    getline(cin,s);
  
    while(t--)
    {
        
        getline(cin,s);
        head=nullptr;
        while(!s.empty())
        {  
            
            stringstream ss(s);
            ss>>wl;
            if(ss.peek()==' ')
            ss.ignore();
            ss>>dl;
            if(ss.peek()==' ')
            ss.ignore();
            ss>>wr;
            if(ss.peek()==' ')
            ss.ignore();
            ss>>dr;
            
        //cout<<dl<<wl<<wr<<dr<<'\n';
        node *temp1=new node(wl,wr,dl,dr);
        insert(head,temp1);   
        getline(cin,s);
        
        }
       
       
        pair<bool,int> temp=traverse(head);
        if(temp.first==false)
        cout<<"NO";
        else
        cout<<"YES";
        cout<<'\n';
        
       
    }
   // pair<bool,int>temp=traverse(head);
    //cout<<temp.first;
    
   

    return 0;

}