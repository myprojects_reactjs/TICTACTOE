#include<bits/stdc++.h>
using namespace std;
int main(){
    int n,t;
    cin>>t;
    while(t--)
    {
        cin>>n;
        vector<int>A(n);
        int j;
        for(int i=0;i<n;i++)
        {
            cin>>A[i];
        }
        int count=2;
        int diff=A[1]-A[0];
        int start=0,end=2;
        for(;end<n;)
        {
            if(diff!=A[end]-A[end-1])
            {
                diff=A[end]-A[end-1];
                start=end-1;
            }
            end++;
    
            count=end-start>count?end-start:count;
        }
        cout<<count<<"\n";;
    }
    return 0;
}