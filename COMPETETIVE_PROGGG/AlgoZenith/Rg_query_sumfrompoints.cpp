#include<bits/stdc++.h>
#define ll long long
/*#define ull unsigned long long
#define lld long double
#define fo(i,s,n) for(int i=s;i<=n;i++)
#define fr(i,n,s) for(int i=n;i>=s;i--)
#define pb push_back
#define mp make_pair
#define F first
#define S second
#define sz(a) a.size()
#define trav(x) for(auto &it:x)
const ll mod = 1000000000+7 ;
//const ll mod = 998244353 ;
const double pi = 3.1415926535897932384626433832795;
typedef pair<ll,ll> pii;
#define ub upper_bound
#define lb lower_bound
typedef vector<ll> vi;
typedef vector<vector<ll>> vvi;
typedef vector<pair<ll, ll>> vpii;
#define all(x) (x).begin()+1, (x).end() //--
#define inf 1e18
#define setp(x) setprecision(x)
#define min(a, b)       ((a) < (b) ? (a) : (b))
#define max(a, b)       ((a) < (b) ? (b) : (a))
#define yes cout<<"YES\n";
#define no cout<<"NO\n";
#define rl(x) cin>>x;
#define rv(x, n) vi x(n+1); fo(i,1,n) cin>>x[i];


void __print(int x) {cerr << x;}
void __print(long x) {cerr << x;}
void __print(long long x) {cerr << x;}
void __print(unsigned x) {cerr << x;}
void __print(unsigned long x) {cerr << x;}
void __print(unsigned long long x) {cerr << x;}
void __print(float x) {cerr << x;}
void __print(double x) {cerr << x;}
void __print(long double x) {cerr << x;}
void __print(char x) {cerr << '\'' << x << '\'';}
void __print(const char *x) {cerr << '\"' << x << '\"';}
void __print(const string &x) {cerr << '\"' << x << '\"';}
void __print(bool x) {cerr << (x ? "true" : "false");}
template<typename T, typename V>
void __print(const pair<T, V> &x) {cerr << "{ "; __print(x.first); cerr << ", "; __print(x.second); cerr << " }";}
template<typename T>
void __print(const T &x) {int f = 0; cerr << " "; for (auto &i: x) cerr << (f++ ? " , " : ""), __print(i); cerr << " ";}
void _print() {cerr << "]\n";}
template <typename T, typename... V>
void _print(T t, V... v) {__print(t); if (sizeof...(v)) cerr << ", "; _print(v...);}
#ifndef ONLINE_JUDGE
#define debug(x...) cerr << "[" << #x << "] = ["; _print(x)
#else
#define debug(x...)
#endif


clock_t time_p=clock();
void timer(){time_p=clock()-time_p;cerr<<"\nTime Taken : "<<(float)(time_p)/CLOCKS_PER_SEC<<"sec\n";}

void swap(ll &x, ll &y) {ll temp = x; x = y; y = temp;}
void google(ll t) {cout << "Case #" << t << ": ";}
ll bimult(ll a , ll b , ll m) {ll res = 0; while(b) {if(b&1) {res = (res + a)%m;}a = (a + a)%m;b>>=1;}return res;}
ll biexp(ll a, ll b , ll m) {ll res = 1;while(b) {if(b&1) {res = res * a;res%=m;}a = a * a;a%=m;b>>=1;}return res;}
inline ll gcd(ll a, ll b) { return b == 0 ? a : gcd(b, a % b); }
inline ll lcm(ll a, ll b) { return a * b / gcd(a, b); }
inline ll ceil(ll a, ll b){  return (a%b==0) ? a/b : a/b +1 ;}
//#define onebits(x)      __builtin_popcountll(x)
ll set_bits(ll n) { ll cnt = 0;while(n) { n-=n&-n; cnt++;}return cnt;}
#define zerobits(x)      __builtin_ctzll(x)
//const ll N = 2000010;
//template<class A> using ordered_set = tree<A,null_type,less<A>,rb_tree_tag,tree_order_statistics_node_update>;
  // find_by_order() , order_of_key()
priority_queue< ll , vector<ll> ,greater<ll> > pq;
// int dx[] = { -2, -1, 1, 2, -2, -1, 1, 2 }; //knight move;
// int dy[] = { -1, -2, -2, -1, 1, 2, 2, 1 };
vi dx{1 , 0 , -1 , 0};
vi dy{0 , 1 , 0 , -1};
//ll dx[] = { -1, -1, -1,  0, 0,  1, 1, 1 };
//ll dy[] = { -1,  0,  1, -1, 1, -1, 0, 1 };
string S1 = "abcdefghijklmnopqrstuvwxyz";
string S2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
*/
using namespace std;

int main()
{
    int i=1e9;
   int n,q;
    ll X,Y,x,y;
    cin>>n>>q;
    vector<ll>nx,ny;
    vector<pair<ll,ll>>queries;
    for(int i=0;i<n;i++)
    {
        cin>>X>>Y;
        nx.push_back(X);
        ny.push_back(Y);

    }
    for(int i=0;i<q;i++)
    {
        cin>>x>>y;
        queries.push_back({x,y});
    }
    sort(nx.begin(),nx.end());
    sort(ny.begin(),ny.end());
    //upper_bound(v.begin(), v.end(),);
    int *nx_sum=new int[n];
    int *ny_sum=new int[n];
    nx_sum[0]=nx[0];
    ny_sum[0]=ny[0];
    for(int i=1;i<n;i++)
    {
        nx_sum[i]=nx[i]+nx_sum[i-1];
        ny_sum[i]=ny[i]+ny_sum[i-1];
    }
    for(int i=0;i<q;i++)
    {
        x=queries[i].first;y=queries[i].second;
        int upper1=upper_bound(nx.begin(),nx.end(),x)-nx.begin();
        int upper2=upper_bound(ny.begin(),ny.end(),y)-ny.begin();
        int x_gr=n-upper1;
        int x_lr=upper1;
        int y_gr=n-upper2;
        int y_lr=upper2;
        ll ans1=upper1*x-(upper1>0?nx_sum[upper1-1]:0)+(nx_sum[n-1]-(upper1>0?nx_sum[upper1-1]:0))-(n-upper1)*x; 
        ll ans2=upper2*y-(upper2>0?ny_sum[upper2-1]:0)+(ny_sum[n-1]-(upper2>0?ny_sum[upper2-1]:0))-(n-upper2)*y;
        cout<<ans1+ans2<<"\n";
    }    
    
   cout<<i;
    return 0;
}