#include<bits/stdc++.h>
#define ll long long
using namespace std;

int main()
{
    int n,t;
    ll j;
    cin>>t;
    
    while(t--)
    {
        ll temp=1;
        cin>>n;
        for(int i=0;i<n;i++)
        {
            cin>>j;
            temp^=j;
        }
        cout<<temp<<" ";
        cout<<"\n";
    }
}