#include<bits/stdc++.h>
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
using namespace std;

signed main()
{
    IOS
    int t;
    cin>>t;
    string s,str,p;
    int a,b,k;
    while(t--)
    {
        cin>>s;
        int q;
        cin>>q;
        int n=s.length();
        while(q--)
        {
            cin>>str;
            if(str=="print")
            {
                cin>>a>>b;
                cout<<s.substr(a,b-a+1)<<'\n';
            }
            else if(str=="reverse")
            {
                cin>>a>>b;
                while(a<b)
                {
                    swap(s[a],s[b]);
                    a++;b--;
                }
            }
            else if(str=="replace")
            {
                cin>>a>>b>>p;
                int k=0;
                for(int i=a;i<=b;i++)
                {
                    s[i]=p[k++];
                }
            }
            else if(str=="rotate")
            {
                cin>>k;
                s = s.substr(n-k,k) + s.substr(0,n-k);
            }
        }
    }
}