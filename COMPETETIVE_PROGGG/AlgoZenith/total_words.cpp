#include<bits/stdc++.h>
#define ll long long
#define mod 1000000007
using namespace std;
ll expo(ll a,ll b)
{
    if(b==0)
        return 1;
    ll temp=expo(a,b/2);
    temp*=temp;
    temp=temp%mod;
    if(b%2==1)
    {
        temp=(a*temp)%mod;
    }
    
    return temp;
}
int main()
{
    int t;
    cin>>t;
    ll n;
    while(t--){
    cin>>n;
    ll temp1,temp2,temp3;
    temp1=expo(26,n);
    temp2=expo(5,n);
    temp3=expo(21,n);
    temp1-=temp2+temp3;
   // cout<<temp1<<" "<<temp2<<" "<<temp3<<'\n';
   if(temp1<0)
   {
       temp1=-temp1;
       temp1=mod-(temp1%mod);
   }
   cout<<temp1<<'\n';
    }
    return 0;
}