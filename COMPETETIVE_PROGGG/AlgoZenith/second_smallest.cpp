#include<bits/stdc++.h>
#define ll long long
#define IOS ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
using namespace std;
int main()
{
    int t;
    cin>>t;
    int n;
    string s;
    while(t--)
    {
        cin>>n;
        getline(cin,s);
        getline(cin,s);
        stringstream ss(s);
        vector<int>input;
        int a;
        while(ss>>a)
        {
            input.push_back(a);
            if(ss.peek()==' ')
                ss.ignore();
        }
        set<int>s(input.begin(),input.end());
        
        set<int>::iterator itr1= s.begin(),itr2=itr1;
        itr1++;
        if(s.size()<2||(*itr1==*itr2))
        cout<<"-1\n";
        else 
        cout<<*(itr1)<<'\n';
        
    }
    return 0;
}