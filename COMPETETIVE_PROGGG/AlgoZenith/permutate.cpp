#include<bits/stdc++.h>
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
using namespace std;
vector<vector<int>>ls;
void permutate(vector<int>& visited,vector<int>&perm,int n,int& count)
{
    if(count==n)
    {
      //  cout<<'\n';
        ls.push_back(perm);
        return;
    }
    for(int i=1;i<=n;i++)
    {
        if(visited[i]==0){
        //cout<<i<<' ';
        perm.push_back(i);
        visited[i]=1;
        ++count;
        permutate(visited,perm,n,count);
        visited[i]=0;
        perm.pop_back();
        --count;
        }
    }

}

int main()
{
    IOS
    int n;
    cin>>n;
    vector<int>visited(n+1,0);
    int count=0;
    vector<int>perm;
    permutate(visited,perm,n,count);
    for(auto i :ls)
    {
        for(auto j :i)
        {
            cout<<j<<' ';
        }
        cout<<'\n';
    }
    return 0;
}