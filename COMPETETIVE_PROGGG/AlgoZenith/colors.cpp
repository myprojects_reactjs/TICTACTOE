#include<bits/stdc++.h>
#include <iostream>
#include<stdio.h>

#define ll long long
#define mod 1000000007
using namespace std;

ll expo(ll a,ll b)
{
    if(b==0)
        return 1;
    ll temp=expo(a,b/2);
    temp*=temp;
    temp=temp%mod;
    
    if(b%2==1)
    {
        temp=(a*temp)%mod;
    }
    
    return temp;
}
ll mul(ll a,ll b)
{
  if(b==0)
  return 0;
  ll temp=mul(a,b/2);
  temp=2*temp;
  if(b%2==1)
  temp+=a;

  temp%=mod;
  return temp;


}

int main()
{
    int t;
    ll n,k,ans;
    cin>>t;
    while(t--)
    {
        cin>>n>>k;
        ans=expo(k-1,n-1);
        ans=mul(k,ans);
        cout<<ans%mod<<'\n';
    }
    return 0;
}