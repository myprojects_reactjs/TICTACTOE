#include<bits/stdc++.h>
using namespace std;

int main()
{
 

int t,n,size,in;
cin>>t;
int i2=1;
while(t--)
{
    vector<int>st1;
    vector<pair<int,int>>st2;
    cin>>size;
    n=size;
    int ind=0;
    while(n--)
    {
        cin>>in;
        st1.push_back(in);
        st2.push_back({in,ind++});

    }

    sort(st2.begin(),st2.end());
    int temp;
    vector<int>ans(size,0);
    for(int i=0;i<size;i++)
    {
        temp=2*st1[i];
        pair<int,int>temp1={temp,0};
        auto i1=lower_bound(st2.begin(), st2.end(), temp1);
        
        if(i1==st2.end()||i1->first>temp)
        i1--;
        if(i1->first<=temp&&i1->second!=i)
        {
            ans[i]=i1->first;
            continue;
        }
        
        else if(i1->second==i)
        {
            if(i1==st2.begin())
            ans[i]=-1;
            else
            {
                i1--;
                ans[i]=i1->first;
            }
        }

    }

    cout<<"Case #"<<i2++<<": ";
    for(int i=0;i<size;i++)
    {
        cout<<ans[i]<<" ";
    }
    cout<<'\n';
}
return 0;

}
