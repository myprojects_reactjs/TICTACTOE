// #include<bits/stdc++.h>
// #define ll long long
// #define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
// using namespace std;

// int main()
// {
//     int t;cin>>t;
//     int n;
//     ll a,b;
//     while(t--){
//     cin>>n;
//     map<ll,ll>sp,ep;
//     for(int i=0;i<n;i++)
//     {
//         cin>>a>>b;
//         sp[a]++;
//         ep[b+1]++;
//     }
//     ll max=INT_MIN,count=0;    
//     for(auto j:sp)
//     {
//         auto end=ep.begin();
//         count+=j.second;
//         if(j.first>=end->first)
//         {
//             for(auto j1:ep)
//             {
//                 if(j.first<j1.first)
//                     break;
//                 count-=j1.second;
//                 ep.erase(j1.first);
//             }
//         }
//         if(count>max)
//             max=count;
//     }
//         cout<<max<<'\n';
        
        
        
//     }
//     return 0;
// }
// Sort the vector of pair according to the time. Arrival would increase the number of customers and departure would decrease the number of customers.

// Time Complexity per test case: O(N log N)

#include <bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

int main() {
    IOS
    int t;
    cin >> t;
    while (t--) {
        int n;
        cin >> n;
        vector<pair<int, int>> v;
        for (int i = 0; i < n; ++i) {
            int x, y;
            cin >> x >> y;
            v.push_back({x, 1});
            v.push_back({y, -1});
        }
        sort(v.begin(), v.end());
        int cur = 0, ans = 0;
        for(auto i:v) {
            cur += i.second;
            ans = max(ans, cur);
        }
        cout << ans << "\n";
    }
    return 0;
}

