#include <bits/stdc++.h>
#define ll long long
using namespace std;



struct sets
{
    set<int> ns, ps, s;
    void insert(int x)
    {
        s.insert(x);
        if (ns.find(x) != ns.end())
            ns.erase(x);
        if (ps.find(x) != ps.end())
            ps.erase(x);
        if (s.find(x + 1) == s.end())
            ns.insert(x + 1);
        if (s.find(x - 1) == s.end())
            ps.insert(x - 1);
    }
    int get_next(int x)
    {
        if (s.find(x) == s.end())
            return x;
        return *(ns.lower_bound(x));
    }
    int get_prev(int x)
    {
        if (s.find(x) == s.end())
            return x;

        auto it = ps.lower_bound(x);
        it--;
        return *it;
    }
};
 sets rows[50001], colms[50001];
int main()
{
  
    int t;
    cin >> t;
    int n, r, c, x, y;
    string s;
    map<char,int>mp;
    mp['N']=0;mp['S']=1;mp['E']=2;mp['W']=3;
    int x1[] = {-1, 1, 0, 0};
    int y1[] = {0, 0, 1, -1};
    int d1;
    while (t--)
    {
        cin >> n >> r >> c >> x >> y;
        cin >> s;
       

        rows[x].insert(y);
        colms[y].insert(x);
        
        for (int i = 0; s[i] != '\0'; i++)
        {
            
            d1=mp[s[i]];
            x += x1[d1];
            y += y1[d1];
            if(s[i]=='N')
            {
         
                x=colms[y].get_prev(x);
           
            }
            else if(s[i]=='S')
            {
                
                x=colms[y].get_next(x);
            
            }
            else if(s[i]=='E')
            {
                y=rows[x].get_next(y);
        
                
            }
            else if(s[i]=='W')
            {
                y=rows[x].get_prev(y);
                
            }
            colms[y].insert(x);
            rows[x].insert(y);

        }
        cout<<x<<" "<<y<<'\n';
    }
    return 0;
}
