#include<bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
using namespace std;
using namespace __gnu_pbds;
typedef tree <pair<int,int>, null_type, less<pair<int,int>>, rb_tree_tag, tree_order_statistics_node_update> ordered_set; //find_by_order,order_of_key

int main()
{
    
    int n,t,a;
    cin>>t;
    while(t--)
    {
       
        ordered_set s;
        int a,count=0;
       	cin>>n;
        for(int i=1;i<=n;i++)
        {
            cin>>a;
            if(count<a)
            s.insert({a,i});
            int b=s.order_of_key({count+1,0});
            if((int)s.size()-b==count+1)
            count++;
            cout<<count<<" ";
            
        }
    }
    return 0;
}