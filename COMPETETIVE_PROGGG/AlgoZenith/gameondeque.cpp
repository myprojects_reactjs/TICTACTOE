#include <bits/stdc++.h>
#define ll long long
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

int main() {
    IOS
    int a,n,q,t;
    cin>>t;
    ll x;
    while(t--)
    {
        cin>>n>>q;
        vector<int>in;
        in.push_back(INT_MIN);
        for(int i=1;i<=n;i++)    
        {
            cin>>a;
            in.push_back(a);
        }
        
        int index,m=INT_MIN;
        deque<int>dq;
        for(int i=1;i<=n;i++)
        {
            if(m<in[i])
            {
                index=i;m=in[i];
            }
        }

        vector<int>in1;
        int min=in[1],max=in[1];
        vector<pair<int,int>>vp;

        for(int i=index+1;i<=n;i++)
        {
            dq.push_back(in[i]);
        }

        
        for(int i=2;i<=index;i++)
        {
            vp.push_back({max,in[i]});
            if(in[i]>max)
            {
                min=max;
                max=in[i];
            }
            dq.push_back(min);
        }
        for(int i=1;i<=q;i++){
        cin>>x;
        if(x<index)
        {
            cout<<vp[x-1].first<<" "<<vp[x-1].second<<'\n';
        }
        else
        {
            cout<<m<<" "<<dq[(x-index)%(n-1)]<<'\n';
        }
        }
    }
    return 0;
}
