#include<bits/stdc++.h>
#define ll long long
#define IOS ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
using namespace std;
int main()
{
    int t;
    cin>>t;
    while(t--)
    {
        int q,i,val;
        set<int>s1,s2;
        cin>>q;
        while(q--)
        {
            cin>>i>>val;
            if(i==1)
              {
                if(s2.find(val)==s2.end())
               {
                  s1.insert(val);
                  s2.insert(val+1);
               }
               else
               {
                   s2.erase(val);
                   s1.insert(val);
                   s2.insert(val+1);
               }
              }
            else 
            {
                if(s1.find(val)==s1.end())
                {
                    cout<<val<<'\n';
                }
                else
                {
                    cout<<*(s2.upper_bound(val))<<'\n';
                }
            }
        }
    }
    return 0;
}