#include<bits/stdc++.h>
#define ll long long
using namespace std;
int main()
{
    ll t,temp=0;
    cin>>t;
    ll a,b,c;
    while(t--)
    {
        cin>>a>>b>>c;
        temp=b*b-4*a*c;
        if(temp==0)
        cout<<"1"<<'\n';
        else if(temp<0)
        cout<<"0"<<'\n';
        else
        cout<<"2"<<'\n';
    }
    return 0;
}