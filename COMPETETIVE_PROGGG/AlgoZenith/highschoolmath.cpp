#include<bits/stdc++.h>
#define ll long long
#define mod 1000000009
using namespace std;
ll expo(ll a,ll b)
{
    if(b==0)
        return 1;
    ll temp=expo(a,b/2);
    temp*=temp;
    temp=temp%mod;
    if(b%2==1)
    {
        temp=(a*temp)%mod;
    }
    
    return temp;
}
ll mul(ll a,ll b)
{
  if(b==0)
  return 0;
  ll temp=mul(a,b/2);
  temp=2*temp;
  if(b%2==1)
  temp+=a;

  temp%=mod;
  return temp;
}
int main()
{
    int t;
    cin>>t;
    ll a,b,n;
    while(t--)
    {
        cin>>a>>b>>n;
        if(a==1)
        {
            ll temp=mul(b,n);
            ll temp1=(1+temp)%mod;
            cout<<temp1<<'\n';
        }
        else
        {
        ll inva=expo(a-1,mod-2);
        ll an=expo(a,n);
        //ll an1=(an-1)%mod;
        ll temp=mul(b,an-1);
        ll temp1=mul(temp,inva);
        ll ans=(an+temp1)%mod;
        cout<<ans<<'\n';
        }
    }
    return 0;

}