#include<bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>

#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
using namespace std;
using namespace __gnu_pbds;
typedef tree <int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_update> ordered_set; //find_by_order,order_of_key


signed main()
{
    IOS
    int t;
    cin>>t;
    int q,a;
    while(t--)
    {
        cin>>q;
        ordered_set s;
        string str;
        while(q--)
        {
            cin>>str;
            if(str=="add")
            {
                cin>>a;
                s.insert(a);
            }
            else if(str=="remove")
            {
                cin>>a;
                ordered_set::iterator itr=s.find(a);
                if(itr!=s.end())
                s.erase(itr);
            }
            else if(str=="find")
            {
                
                cin>>a;
                if(a >= (int)s.size()) {
                    cout << "-1\n";
                }
                else
                cout<<*(s.find_by_order(a))<<'\n';
                
            
               
            }
            else if(str=="findpos")
            {
                cin>>a;
                cout<<s.order_of_key(a)<<'\n';
            }
        }
    }
    
}