#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;
    cin>>t;
    int x1,x2,y1,y2,x3,x4,y3,y4;
    while(t--)
    {
        cin>>x1>>y1>>x2>>y2;
        cin>>x3>>y3>>x4>>y4;
        //CONDS FOR X
        int x_dis=0,y_dis=0;
        if(x4<=x1||x2<=x3)
        {
            x_dis=0;
        }
        else if(x1<=x3&&x4<=x2)
        
        {
           x_dis=x4-x3;
        }
        else if(x3<=x1&&x2<=x4)
        {
              x_dis=x2-x1;
        }
        else if(x1<=x4&&x4<=x2)
        {
            x_dis=x4-x1;
        }
        else if(x1<=x3&&x3<=x2)
        {
            x_dis=x2-x3;
        }
        
         //CONDS FOR Y
        if(y4<=y1||y2<=y3)
        {
            y_dis=0;
        }
        else if(y1<=y3&&y4<=y2)
        {
            y_dis=y4-y3;
        }
        else if(y3<=y1&&y2<=y4)
        {
        	y_dis=y2-y1;
        }
        else if(y1<=y4&&y4<=y2)
        {
            y_dis=y4-y1;
        }
        else if(y3<=y2&&y3<=y2)
        {
            y_dis=y2-y3;
        }
        int intersec=0,union_rec=0;
        intersec=y_dis*x_dis;
        union_rec=(y4-y3)*(x4-x3)+(y2-y1)*(x2-x1)-intersec;
        
        cout<<intersec<<" "<<union_rec<<"\n";
    }
    
    return 0;
}