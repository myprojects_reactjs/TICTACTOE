#include<bits/stdc++.h>
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
using namespace std;

int main()
{
    IOS
    int t;
    cin>>t;
    string s;
    int n;
    while(t--)
    {
        cin>>n;
        getline(cin,s);
        getline(cin,s);
        stringstream ss(s);
        vector<int>input;
        int a;
        while(ss>>a)
        {
            input.push_back(a);
            if(ss.peek()==' ')
                ss.ignore();
        }
        multiset<int>ms;
        ms.insert(input[0]);
        int count=1;
        for(int i=1;i<n;i++)
        {
            int key=input[i]+1;
            multiset<int>::iterator itr=ms.lower_bound(key);
            if(itr==ms.end())
            {
                count++;
                ms.insert(input[i]);
            }
            else
            {
                ms.erase(itr);
                ms.insert(input[i]);
            }

        }
        cout<<count<<'\n';


    }
    return 0;
}