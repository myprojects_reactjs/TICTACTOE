#include<bits/stdc++.h>
#define ll long long
#define mod 1000000007
using namespace std;
vector<ll>invfact(100001,0);
vector<ll>fact(100001,0);
ll mul(ll a,ll b)
{
  if(b==0)
  return 0;
  ll temp=mul(a,b/2);
  temp=2*temp;
  if(b%2==1)
  temp+=a;

  temp%=mod;
  return temp;
}

ll expo(ll a,ll b)
{
    if(b==0)
        return 1;
    ll temp=expo(a,b/2);
    temp*=temp;
    temp=temp%mod;
    if(b%2==1)
    {
        temp=(a*temp)%mod;
    }
    
    return temp;
}
ll ncr(ll a,ll b)
{
    if(b==a)
    return 1;

    ll temp=invfact[a-b]*invfact[b]%mod;
    temp=temp*fact[a]%mod;
    return temp;
}
int main()
{
    int n,q,k;
    cin>>n;
    string s;
    getline(cin,s);
    getline(cin,s);
    vector<ll>input2;
    fact[0]=1;
    for(ll i=1;i<=n;i++)
    {
        fact[i]=fact[i-1]*i;
        fact[i]%=mod;
    }
    //factorials collected
    ll temp=expo(fact[n],1000000005);
   
    invfact[n]=temp;
    for(ll i=n-1;i>=1;i--)
    {
        invfact[i]=(i+1)*invfact[i+1]%mod;
    
    }
    // inv fact is collected

    vector<ll>da(n+1,0),da1(n+1,0);

    da[0]=1;da[1]=0;da[2]=invfact[2];

    da1[2]=mul(fact[2],da[2]);
    da1[2]=mul(da1[2],ncr(n,2));
    da1[2]=(da1[2]+1)%mod;
    for(ll i=3;i<=n;i++)
    {
        if(i%2==1)
        da[i]=da[i-1]-invfact[i];
        else
        da[i]=da[i-1]+invfact[i];

        if(da[i]<0)
        {
            ll temp=-da[i];
            temp%=mod;
            temp=mod-temp;
            da[i]=temp;
        }
        da1[i]=mul(fact[i],da[i]);
        da1[i]=mul(ncr(n,i),da1[i]);
        da1[i]=(da1[i]+da1[i-1])%mod;
       
    }

    cin>>q;
    ll in;
    da1[0]=1;
    da1[1]=1;
    for(int i=0;i<q;i++)
    {
          cin>>in;
          
          if(in>n)
          in=n;

          cout<<da1[in]<<'\n';

    }


    return 0;
}