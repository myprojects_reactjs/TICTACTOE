#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

int main() {
    IOS
    int n,t;
    cin >> t;
    while (t--) {
        cin >> n;
    	string s;
        getline(cin,s);
        getline(cin,s);
        ll a;
        stringstream ss(s);
        vector<ll>in;
        in.push_back(0);
        while(ss>>a)
        {
            in.push_back(a);
            if(ss.peek()==' ')
                ss.ignore();
        }
        int start=1,max=INT_MIN,temp;
        map<int,int>mp;
        for(int end=1;end<=n;end++)
        {
            if(mp[in[end]]!=0)
            {
                temp=start;
                start=mp[in[end]]+1;
                for(int j=mp[in[end]];j>=temp;j--)
                    mp[in[j]]=0;
                
            }
            mp[in[end]]=end;
            max=max<end-start+1?end-start+1:max;
        }
        cout<<max<<'\n';
    }
    return 0;
}