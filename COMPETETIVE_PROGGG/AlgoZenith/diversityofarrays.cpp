#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
ll nc2(int n)
{
    return (n%2==1?n*((n-1)/2):(n/2)*(n-1));
    
}
struct p
{

ll index,count;
p(ll count=0,ll index=0):count{count},index{index}{}
} ;
 struct cmp
{
    bool operator()(p o1,p o2)
    {
        return o1.count<o2.count;
    }
};
 
int main()
{
  
    IOS;
     
    ll t,k,n,a;
    cin>>t;

    while(t--)
    {
        map<ll,ll>mp1,mp2;
        priority_queue<p,vector<p>,cmp>pq;
        
        cin>>n>>k;
        ll count=0;
        for(int i=1;i<=n;i++)
        {
            cin>>a;
            if(mp1[a]==0)
                mp1[a]++;
            else
            {
                mp2[a]++;
                count++;
            }
        }

        for(auto i:mp2)
        {
            pq.push({i.second,i.first});
        }
        ll size=mp1.size();
        if(k<count)
        {
            p in1,in2;
            in1=pq.top();

            while(!pq.empty()&&k!=0)
            {

            pq.pop();
            in2=pq.top();
            ll temp=in1.count-in2.count+1;ll temp2;
            if(k<=temp)
            {
                size+=k;
                count-=k;
                temp2=in1.count-k;
                k=0;
                
            
            }
            else
            {
                size+=temp;
                temp2=in1.count-temp;
                k-=temp;
                count-=temp;
            }
           // cout<<count<<" "<<k<<'\n';
          
            if(temp2>0){
            pq.push({temp2,in1.index});
            mp2[in1.index]=temp2;
            }
            else
            mp2.erase(mp2.find(in1.index));

            in1=in2;
            
            }
            ll ans=0;
            for(auto i:mp2)
            {
               
                ans+=(i.second)*(count-i.second);
            }
            ans/=2;
            
            ans+=nc2(size);
            ans+=(count*(size-1));
            cout<<ans<<'\n';

        }
        else
        {
            size+=count;
            cout<<nc2(size)<<'\n';
        }
        
        
    }
    return 0;
}