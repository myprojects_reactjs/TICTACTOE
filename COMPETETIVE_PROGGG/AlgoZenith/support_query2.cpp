#include<bits/stdc++.h>
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
using namespace std;


struct st
{
    //priority_queue<int,vector<int>,less<int>>pq1;
    set<int>pq1,pq2;
   
    //priority_queue<int,vector<int>pq1,greater<int>>pq2;
    map<int,int>mp1,mp2;
    int count=0;
    int sum=0;
    void add(int x,int k)
  	{
       
        if(count<k)
        {
            mp1[x]++;
            pq1.insert(x);
            count++;
            sum+=x;
        }
        else
        {
            if(*pq1.begin()<x)
            {
                auto a=pq1.begin();
                pq2.insert(*a);
                mp1[*a]--;
                if(mp1[*a]==0)
                    pq1.erase(a);

                mp2[*a]++;
                mp1[x]++;
                pq1.insert(x);
                sum-=(*a);
                sum+=x;
            }
            else
            {
                pq2.insert(x);
                mp2[x]++;
            }
        }
     //  cout<<sum<<'\n';
  	}
    void remove(int x)
    {
        auto a=pq2.find(x);
        if(a!=pq2.end())
        {
            mp2[*a]--;
            if(mp2[*a]==0)
            pq2.erase(a);
        }
        else
        {
            
            a=pq1.find(x);
            if(a!=pq1.end())
            {
                
                sum-=x;
                mp1[x]--;

                if(mp1[x]==0)
                pq1.erase(a);
                count--;

                if(!pq2.empty())
                {
                    auto b=pq2.end();
                    b--;
                    pq1.insert(*b);
                    mp1[*b]++;
                    sum+=(*b);
                    count++;
                    mp2[(*b)]--;
                    if(mp2[(*b)]==0)
                    {
                        pq2.erase(b);
                    }
                }

            }
        }


        
    }


	int sum1()
    {
        return sum;
    }
};


int main()
{
    IOS
    int q,k;
    cin>>q>>k;
    char a,b;
    st s;
    for(int i=1;i<=q;i++)
    {
        cin>>a>>b;
        if(a=='1')
        {
            s.add(b-'0',k);
        }
        else if(a=='2')
        {
            s.remove(b-'0');
        }
        else
        {
            cout<<s.sum1()<<"\n";
        }
    }
    return 0;
}