#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

int main() {
    IOS
    int t;
    cin>>t;
    int n,m;
    while(t--)
    {
        cin>>n>>m;
        vector<int>a,b;
        int in;
        for(int i=0;i<n;i++)
        {
            cin>>in;
            a.push_back(in);
        }
        for(int i=0;i<m;i++)
        {
            cin>>in;
            b.push_back(in);
        }
        sort(a.begin(),a.end());sort(b.begin(),b.end());
        set<int>s;
        for(int i=0;i<n;i++)
        {
            s.insert(a[i]);
        }
        for(int i=0;i<m;i++)
        {
            s.insert(b[i]);
        }
        vector<int>intersect,diff;
        for(int i=0;i<n;i++)
        {
            if(binary_search(b.begin(),b.end(),a[i]))
            {
                intersect.push_back(a[i]);
            }
            else
            {
                diff.push_back(a[i]);
            }
        }

        for(auto i:s)
        cout<<i<<' ';
        cout<<'\n';

        for(auto i:intersect)
        cout<<i<<' ';
        cout<<'\n';

        for(auto i:diff)
        cout<<i<<' ';
        cout<<'\n';


    }
    return 0;
}