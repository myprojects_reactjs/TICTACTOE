#include<bits/stdc++.h>
using namespace std;
#define pi 3.141592653589793238

double expo(double a,int b)
{
    if(b==0)
    return 1;
    
    double ans=expo(a,b/2);
    ans=ans*ans;
    if(b%2==1)
    ans*=a;
    return ans;
    
}

int main()
{
    int t;
    cin>>t;
    double r,a,b;
    double temp,temp1,temp2,ans;
    
    while(t--)
    {
     cin>>r>>a>>b;
     temp=b/a;
   
     ans=pi*r*r;
     ans=ans*(1+a*a);
     int i;
    for( i=2;ans!=0;i+=2)
    {
        temp1=expo(temp,i);
    
        if(temp1>r*r)
            break;
    }
    i=i/2;//success
    temp1=a*a;
    temp2=b*b;
    temp1=temp1/temp2;
    temp2=temp1;
    temp1=expo(temp1,i);
    temp1=(1-temp1)/(1-temp2);
    cout<<fixed<<setprecision(6);
    cout<<ans*temp1<<"\n";
     
    }
    return 0;
}
