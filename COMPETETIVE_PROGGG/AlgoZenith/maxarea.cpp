#include <bits/stdc++.h>
#define ll long long
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);

int main() {
    IOS
    int t,h,p,w,q,h1,w1;
    pair<int,int>hl,vl;
    cin >> t;
    char d;
    while (t--) {
        cin>>h>>w>>q;
        hl.first=0;vl.first=0;
        hl.second=w;
        vl.second=h;
        set<int>hp,vp;
        hp.insert(0);vp.insert(0);
        hp.insert(w); 
        vp.insert(h);
        h1=h;w1=w;
        for(int i=1;i<=q;i++)
        {
            cin>>d>>p;
            if(d=='V')
            {
                hp.insert(p);
                if(p>hl.first&&p<hl.second)
                {
                    int temp1=0,temp2=0;
                    for(auto j:hp)
                    {
                        if(j-temp1>temp2)
                           { 
                               temp2=j-temp1;
                               hl.first=temp1;
                               hl.second=j;
                           }
                        temp1=j;
                    }
                    w1=temp2;
                
                }
                
            }
            else
            {
                vp.insert(p);
                if(p>vl.first&&p<vl.second)
                {
          
                    int temp1=0,temp2=0;
                    for(auto j:vp)
                    {
                        if(j-temp1>temp2)
                           { 
                               temp2=j-temp1;
                               vl.first=temp1;
                               vl.second=j;
                           }
                        temp1=j;
                    }
                    h1=temp2;
                
                }
                
            }
            cout<<(ll)h1*w1<<'\n';
        }
     
    }
       return 0;
}