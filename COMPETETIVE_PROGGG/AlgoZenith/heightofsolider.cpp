#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
int main()
{
    int a,n,t;
    cin>>t;
    while(t--)
    {
        cin>>n;
        cin>>a;
        deque<pair<int,int>>dq;
        dq.push_back({a,1});
        vector<int>arr(n+1,0);
        for(int i=2;i<=n;i++)
        {
            cin>>a;
            if(dq.empty()||dq.back().first<a)
            {
                dq.push_back({a,1});
                if(arr[1]<a)
                arr[1]=a;
            }
            else 
            {
                int s1=1;
                int f=dq.back().first,s=dq.back().second;
                while(!dq.empty()&&f>=a)
                {
                    if(s==1){
                        if(arr[s1]<f)
                            arr[s1]=f;
                        dq.pop_back();
                        s1++;
                        
                    }

                    else
                    
                    {
                        if(s<=s1)
                        {
                            for(int j=max(s1,s);j<s1+s;j++)
                            {
                                if(arr[j]<f)
                                arr[j]=f;
                            }
                            

                        }

                        else
                        {
                            for(int j=min(s1,s);j<s1+s;j++)
                            {
                                if(arr[j]<f)
                                arr[j]=f;
                            }
                        }
                            s1+=s;
                            if(arr[s1]<a)
                            arr[s1]=a;
                            dq.pop_back();
                    }  
                    
                    if(!dq.empty())
                    {f=dq.back().first,s=dq.back().second;}
                }
                if(arr[s1]<a)
                    arr[s1]=a;
                dq.push_back({a,s1});
            }
          
        }

    while(!dq.empty()&&dq.size()!=1)
    {
        pair<int,int>p1=dq.back(),p2;
        dq.pop_back();
        p2=dq.back();


        for(int j=max(p1.second,p2.second)+1;j<=p1.second+p2.second;j++)
        {
            if(arr[j]<p2.first)
            arr[j]=p2.first;
        }
        dq.back().second=p1.second+p2.second;
    }
    arr[dq.back().second]=dq.back().first;

    for(int i=1;i<=n;i++)
   { cout<<arr[i]<<" ";}
    cout<<'\n';
    }
    return 0;
}