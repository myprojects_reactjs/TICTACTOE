
#include<bits/stdc++.h>
using namespace std;
class xy
{
    public:
    int first,second;
    xy(int x,int y):first{x},second{y}{}
};


class cmp
{
    public:
    bool operator()(const xy x1,const xy x2) const
    {
        if(x1.first < x2.first)
            return true;
        return false;
        
        
    }
    
};
class Solution {
public:
    
    
    vector<vector<int>> getSkyline(vector<vector<int>>& buildings) {
     
        set<xy,cmp>cd;
        int size1=buildings.size();
        int lt,rt,ht,x1,x2,h1,h2;
        for(int i=0;i<size1;i++)
        {
            
            lt=buildings[i][0];rt=buildings[i][1];ht=buildings[i][2];
            //cd.insert({x,y});
            if(cd.empty())
            {
                cd.insert({lt,ht});
                cd.insert({rt,0});
                
            }
            
            else
            {

                    
            
                auto lb=cd.upper_bound({lt,INT_MIN}); // points to lt,h if present else <=lt+1,h
                x1=lb->first;h1=lb->second;
               
                auto ub=cd.upper_bound({rt,INT_MAX}); // points to rt,h if present else <=rt+1,h,end
                ub--;
                x2=ub->first;h2=ub->second;
                
                if(lb==cd.end())
                {
                    cd.insert({lt,ht});
                    cd.insert({rt,0});
                    continue;
                }
                
               // lb=ln;
                
                if(ub==cd.begin())
                {
                    
                  cd.insert({lt,ht}); 
                    if(rt!=x2)
                  cd.insert({rt,0});
                    continue;
                }
                
                // auto rn=ub;
                // if(rn==cd.end())
                // rn--;
                // ub=rn;
                //lb ub contain includes [<=lb,<=rb]
                auto pv=lb;
                if(pv!=cd.begin())
                pv--;
                
                int px=-1;
                px=pv->first;
                int ph=-1;
                ph=pv->second;
                
                // lb points to lt,h if present else <=lt+1,h 
                //  rb points to strict <=rt+1,h or endddd
                for(auto tv=lb;tv!=rb;tv++)
                {
                    int nx=tv->first;
                    int nh=tv->second;
                    if(nh<ht)
                    {
                        if(ph>ht&&rt>nx)
                        {
                            cd.erase(tv);
                            cd.insert({nx,ht});
                             ph=ht;
                        }
                        else if(ph<=ht && rt>nx)
                        {
                            cd.erase(tv);
                            ph=ht;
                        }
                        else if(ph<=ht && rt==nx)
                        {
                            ph=ht;
                        }
                        else if(ph<=ht && rt<nx)
                        {
                            ph=nh;
                        }
                        
                    }
                    else
                    {
                        ph=nh;
                    }
               
                    
                    
                }
                
                lb=cd.lower_bound({lt,0});
                ln=lb;
                if(ln!=cd.begin())
                    ln--;
                
                if(ln->second<ht && ht>lb->second)
                    cd.insert({lt,ht});
                ub=cd.upper_bound({rt,0});
                
            }
            
            
            
        }
        
        for(auto i:cd)
        {
            cout<<i.first<<":"<<i.second<<"\n";
        }
        return {{}};
        
    }
};