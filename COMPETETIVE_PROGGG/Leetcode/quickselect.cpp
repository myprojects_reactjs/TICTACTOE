 #include<bits/stdc++.h>
 using namespace std;
 map<int,int>mp;
 int partition(vector<int>&qk,int start,int end)
    {
        
        int pivot=start,i=start+1,j=end;
        
        while(i<=j)
        {
            while(i<=end&&mp[qk[pivot]]>=mp[qk[i]])
            {
                i++;
            }
            while(j>=start&&mp[qk[pivot]]<mp[qk[j]])
            {
                j--;
            }
            if(i<j)
            {
                cout<<"1 "<<qk[i]<<' '<<qk[j]<<'\n';
                swap(qk[i],qk[j]);
            }
        }
        
        if(i>j)
        {
            cout<<"2 "<<qk[pivot]<<' '<<qk[j]<<'\n';
            swap(qk[pivot],qk[j]);
        }
        return j;
        
    }// to sort the elements
    
    int quickselect(vector<int>&qk,int start,int end,int k) // return index
    {
        if(start>=end)
         return start;
        
        int pivot=start+ rand()%(end-start+1);
        
        swap(qk[pivot],qk[start]);
        
        pivot=partition(qk,start,end);
        
        if(pivot==k)
            return pivot;
        
        if(k<pivot){
           return quickselect(qk,start,pivot-1,k);
        }
        else
           return quickselect(qk,pivot+1,end,k);
        
            
    }