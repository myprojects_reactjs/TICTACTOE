#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll mod=2;
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
ll expo(ll a,ll b)
{
    if(b==0)
        return 1;
    ll temp=expo(a,b/2);
    temp*=temp;
    temp=temp%mod;
    if(b%2==1)
    {
        temp=(a*temp)%mod;
    }
    
    return temp;
}
int main() {
    IOS
    int t;
    cin>>t;
    ll a,b,c,p;
    while(t--)
    {
        cin>>a>>b>>c>>p;
    	mod=p;
        
        if(b==0&&c==0)
        {
            
            cout<<a<<'\n';
           
        }
        else if(b==0)
        {
            cout<<"1\n";
        }
        else
        {
        	if(a==0||a==1)
                cout<<a<<'\n';
            else
            {
                mod=p-1;
                ll power=expo(b,c);
                mod=p;
            a=expo(a,power);
        	cout<<a<<'\n';
            }
        }
            
    }
    
    
    return 0;
}