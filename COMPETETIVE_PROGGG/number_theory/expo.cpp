#include<bits/stdc++.h>
#define ll long long
#define mod 1000000007
using namespace std;
ll expo(ll a,ll b)
{
    if(b==0)
        return 1;
    ll temp=expo(a,b/2);
    temp*=temp;
    temp=temp%mod;
    if(b%2==1)
    {
        temp=(a*temp)%mod;
    }
    
    return temp;
}