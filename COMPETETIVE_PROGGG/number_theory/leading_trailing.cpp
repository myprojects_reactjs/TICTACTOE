#include<bits/stdc++.h>
using namespace std;

int main()
{
    int t, n, i, j;

    cin >> t;
    while (t--)
    {
        cin >> n;
        vector<int> X,Y;
        for(i=0;i<n;i++)
        {
            int x,y;
            cin>>x>>y;
            X.push_back(x);
            Y.push_back(y);
        }
        sort(X.begin(),X.end());
        sort(Y.begin(),Y.end());

        pair<int,int> meet;

        i = (n-1)/2;
        meet.first = X[i];
        meet.second = Y[i];

        long long cost=0;
        for(i=0;i<n;i++)
        {
            cost+=abs(meet.first - X[i]);
            cost+=abs(meet.second- Y[i]);
        }

        cout<<cost<<'\n';
    }
    return 0;
}
