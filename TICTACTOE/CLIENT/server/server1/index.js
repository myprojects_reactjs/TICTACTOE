
import { createRequire } from 'module';
import { socket2 } from '../../src/socket.js';

const require = createRequire(import.meta.url);
const express = require('express');
const http    = require('http');
const {Server} = require('socket.io');
// const {sessionstore}=require('../sessionstore.cjs')
const {findRoomplayer,deleteSessionMsg,updateMessageplayer1,updateMessageplayer2,saveMessage, updatequitplayer,updaterealdisconnectplayer1,updaterealdisconnectplayer2,updatefakedisconnectplayer1,updatefakedisconnectplayer2}=require('../messagestore.cjs')

const app = express();
const server=http.createServer(app);
const io=new Server(server, { 
  cors: {
    origin: "http://localhost:3000",
  },
});



  function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }

  function updatestore(roomid,user_status,opp_status,board,position,sym)
  {
    console.log( `updatestor is called`)
          if(sym==='X'){
            
          updateMessageplayer1(roomid,board,position,user_status,opp_status);
          }
          else if(sym==='O')
          {
           updateMessageplayer2(roomid,board,position,user_status,opp_status);
          }
  }
  
  function draw1(position)
  {
    let sum=0;
    for(let i=0;i<9;i++)
    {
      sum+=position[i];
    }
    if(sum===5)
    return 1;

    return 0;
  }

  let sessionid,username,oppname;
  io.use((socket,next)=>{

   // sessionid=socket.handshake.auth.sessionid;
   console.log('socket2 connection');
  //  roomid=socket.handshake.auth.roomid;
  //  if(checkroom(socket,roomid))
    next();

  //  else
  //  return next();

  })

  
  
   io.on("connection",(socket)=>{

    console.log('socket2 connection');
    socket.on('gamedata',(gamedata)=>{

      
      console.log('gamedata received');

      const position=gamedata.position;
      const roomid=gamedata.roomid
      const cellid=gamedata.cellid;
      const userid=gamedata.userid;
      const board=gamedata.board;
      const sym=gamedata.usym;
      let win,loss,draw;
      console.log(`userid is ${userid} board is ${board} roomid is ${roomid} cellid is ${cellid} position is ${position}`)
      
      if(calculateWinner(position))
      {
       
        socket.to(roomid).emit('status',{
          turn:false,
          cellid:cellid,
          status:2
        });
        io.to(userid).emit('status',{
          status:1
        });

        updatestore(roomid,1,2,board,position,sym);
        
      }
      else if(draw1(position))
      {


        socket.to(roomid).emit('status',{
          turn:false,
          cellid:cellid,
          status:3
        })
        socket.emit('status',{status:3
        });
        updatestore(roomid,3,3,board,position,sym);

      }
      else
      {
        console.log(`sending data to opposite player`)
        if(cellid>=0){
          console.log('went insdie')
        socket.to(roomid).emit('turn',{
          turn:true,
          cellid:cellid
        }) // this has to be sent to the player1 using to after setting session
        updatestore(roomid,0,0,board,position,sym);
      }
     
    }
    socket.emit('playerdisconnect',{
      msg:'player connected'
    });
    if(socket.handshake.auth.player==='1')
    {
      updatefakedisconnectplayer1(roomid,0);
    }
    else
    {
      updatefakedisconnectplayer2(roomid,0);
    }

    });

    socket.on('disconnect',()=>{
      //add condition for unintentional login
      const ssid=socket.handshake.auth.sessionid;
      const player=socket.handshake.auth.player
      const roomid=socket.handshake.auth.roomid;
      io.to(roomid).emit('playerdisconnect',{
        msg:'player disconnected'
      })
      if(player==='1')
      {
        updatefakedisconnectplayer1(roomid,1);
      }
      else
      {
        updatefakedisconnectplayer2(roomid,1);

      }
      //updatefakedisconnectplayer1
      //updatefakedisconnectplayer2
      socket.disconnect(true);
    });


    socket.on('quitting',(data)=>{
      console.log('user intentionally quitting') //player1 pings quitting
      const roomid=data.roomid;
      const player=socket.handshake.auth.player;
      if(player==='1')
      {
        updatequitplayer(roomid);
        updaterealdisconnectplayer1(roomid);
      }
      else
      {
        updatequitplayer(roomid);
        updaterealdisconnectplayer2(roomid);

      }
      if(!roomid)
      roomid=socket.handshake.auth.roomid;
      socket.to(roomid).emit('quit');
      socket.disconnect(true);

    })

    socket.on('join',(data)=>{
      console.log('join logged')
      const playerid=data.userid;
      const roomid=data.roomid;
      socket.join(playerid);
      socket.join(roomid);
    })

   

    
  });
   
    

server.listen(3001, () => {
    //console.log(`dirname is ${__dirname}`)
    console.log('Server listening at port %d', 3001);
  });

  //"start": "nodemon ./server/index.js",
  //"node server/index.js & server/server1/index.js && fg"
  
  