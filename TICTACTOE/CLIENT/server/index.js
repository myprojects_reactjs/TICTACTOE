import path from 'path';
import { fileURLToPath } from 'url';
import { createRequire } from 'module';


//ngnix, sticky sessions  
//match making, heartbeat, disconnection doneeee
// game logic correction   doneee


const require = createRequire(import.meta.url);
const express = require('express');
const http=require('http');
const serveStatic = require('serve-static');
const {Server} = require('socket.io');
//const AsyncLock = require('async-lock');
const {handleConnection,checkSession,deleteRedis,updatesession}=require('./roomcreation.cjs')
//const {fun}=require('./redisconnection.cjs');
const app = express();
const server=http.createServer(app);
const io=new Server(server);
const port = process.env.PORT || 3000;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


app.use(serveStatic(path.join(__dirname,'../build')));
app.use(express.json());
const pendingrooms=new Map();
let sessionid='', username='',roomid='';

io.use(async (socket, next) => {

    sessionid = socket.handshake.auth.sessionid;
    username = socket.handshake.auth.username;
    roomid = socket.handshake.auth.roomid;
    console.log(username);
    if(!sessionid)
    {
      console.log('no session');
      next();
      // use handleconnection fn to create player room, make single player wait, 
      // if other player joins the game then send oppname, sessionid,userid of both players,
    }
    else if(sessionid)
    {
      console.log('check session');

if(pendingrooms[roomid]&&pendingrooms[roomid].sessionid===sessionid)
  {

        if(pendingrooms[roomid])
      clearTimeout(pendingrooms[roomid].timer)
  }
  if(checkSession(sessionid,roomid,socket))
  {
    console.log(`passed checksession so obj is really connected`)
     next();
  }
  else
  {
    console.log('returning null session')
        socket.emit('session',{
          sessionid:null
        })
        return next();
  }
  
      //try to get the session objj frm session store
      //get the session from the store,sent connect event with exisiting data or new data if session expired
    }

  else
  {
    socket.emit('session',{
      sessionid:null
    })
    return next();
  }
    
  });



io.on("connection",  (socket) => {

  //fun(socket);

  if(!sessionid)
  {
    handleConnection(username,socket);
  }

  
    socket.conn.once("upgrade", () => {
      // called when the transport is upgraded (i.e. from HTTP long-polling to WebSocket)
      console.log("upgraded transport", socket.conn.transport.name); // prints "websocket"
    });

    socket.on('quit',(data)=>{
      const ssid=data.sessionid;
      const roomid=data.roomid;
      console.log(`deleting redis obj ssid ${ssid} roomid ${roomid}`);
      deleteRedis(ssid,roomid); // this can be shifted to  below ssid check 
      console.log('user intentionally quitting')
      if(ssid)
      socket.disconnect(true);
    })

//transport close
    socket.on('disconnect', (data) => {
      console.log(`socket or clientdisconnect logged ${data}`);
      const ssid=socket.handshake.auth.sessionid;
      const roomid=socket.handshake.auth.roomid;
      updatesession(ssid,false);
      if(data==='io client disconnect') // need to check what is the error for client unintentional logout
      {
        console.log('setting timer for unintentional quitt')
        // io.to(roomid).emit('playerdisconnect',{
        //   msg:'player disconnected'
        // })
        if(!pendingrooms[roomid]){
          
          pendingrooms[roomid]={sessionid:ssid}
          pendingrooms[roomid].timer=setTimeout(()=>{
          console.log(`deleting redis obj from late session`);
          deleteRedis(ssid,roomid);
          io.to(roomid).emit('forcequit');
          delete pendingrooms[roomid];
        
        },40000);
      }
      else if(pendingrooms[roomid])
      {
        clearTimeout(pendingrooms[roomid].timer);
        deleteRedis(ssid,roomid);
        delete pendingrooms[roomid];
      }
        socket.disconnect(true);
      }

      else if(data!=="transport close" && data!=='transport err')
      {
        console.log(`server disconnect called with this err ${data} so killing player sessions`)
        deleteRedis(ssid,roomid);
        socket.disconnect(true);
      }


    });

    
  });

      
server.listen(port, () => {
  console.log(`dirname is ${__dirname}`)
  console.log('Server listening at port %d', port);
});

app.use((err,req,res,next)=>{
  if(err)
  res.status(400).send(err.message);
  return next();
});


















// socket.on('sample',(data)=>{
//   console.log(`data msg from renderr is ${data.msg}`)
// })










 //console.log(socket);
    //console.log(`sessionid is ${sessionid} ${socket.handshake.auth.ssid}`)
    // if(!sessionid)
    // {
    //   handleConnection(socket);

    // }