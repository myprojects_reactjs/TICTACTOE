

const TTL = 8 * 60;
//const {client}=require('./redisconnection.cjs')

// //
const redis = require('redis');
const client = redis.createClient({
   socket: {
       host: '127.0.0.1',
       port: '6379'
   },
 });
 client.connect();
//to save room data


  async function deleteSessionMsg(id)
  {
    console.log(`delete is called room:${id}`)
    await client.hDel(`room:${id}`,
    'player1_turn')
    await client.hDel(`room:${id}`,
    'player2_status')
    await client.hDel(`room:${id}`,
    'player1_status')
    await client.hDel(`room:${id}`,
    'player2_userid')
    await client.hDel(`room:${id}`,
    'player1_userid')
    await client.hDel(`room:${id}`,
    'player1_position')
    await client.hDel(`room:${id}`,
    'player2_position')
    await client.hDel(`room:${id}`,
    'board')
    await client.hDel(`room:${id}`,
    'player2_turn')
    await client.hDel(`room:${id}`,
    'player1_quit')
    await client.hDel(`room:${id}`,
    'player2_quit')
    await client.hDel(`room:${id}`,
    'player1_realdisconnect')
    await client.hDel(`room:${id}`,
    'player2_realdisconnect')
    await client.hDel(`room:${id}`,
    'player1_fakedisconnect')
    await client.hDel(`room:${id}`,
    'player2_fakedisconnect')
    
  }

  //quit is for respective players, fake and real disconnects should be sent to opposite players

  async function updatequitplayer(id)
  {
    await  client.hSet(`room:${id}`,"player1_quit",1);
    await  client.hSet(`room:${id}`,"player2_quit",1);

  }
  
  async function updatefakedisconnectplayer1(id,disconnect)
  {
    await  client.hSet(`room:${id}`,"player1_fakedisconnect",disconnect);

  }
  async function updatefakedisconnectplayer2(id,disconnect)
  {
    await  client.hSet(`room:${id}`,"player2_fakedisconnect",disconnect);

  }
  async function updaterealdisconnectplayer1(id)
  {
    await  client.hSet(`room:${id}`,"player1_realdisconnect",1);

  }
  async function updaterealdisconnectplayer2(id)
  {
    await  client.hSet(`room:${id}`,"player2_realdisconnect",1);

  }
  


  async function updateMessageplayer1(id,board,position,user_status,opp_status)
  {
 
    console.log(`roomid is room:${id}`)
    await  client.hSet(`room:${id}`,"player1_turn","false")
    await  client.hSet(`room:${id}`,"player2_turn","true")
    await client.hSet(`room:${id}`,"board",JSON.stringify(board));
    await client.hSet(`room:${id}`,'player1_position',JSON.stringify(position));
    await client.hSet(`room:${id}`,'player1_status',JSON.stringify(user_status));
    await client.hSet(`room:${id}`,'player2_status',JSON.stringify(opp_status));
  }
  async function updateMessageplayer2(id,board,position,user_status,opp_status)
  {
    console.log(`roomid is room:${id}`)
    await  client.hSet(`room:${id}`,"player1_turn","true")
    await  client.hSet(`room:${id}`,"player2_turn","false")
    await client.hSet(`room:${id}`,"board",JSON.stringify(board));
    await client.hSet(`room:${id}`,'player2_position',JSON.stringify(position));
    await client.hSet(`room:${id}`,'player1_status',JSON.stringify(opp_status));
    await client.hSet(`room:${id}`,'player2_status',JSON.stringify(user_status));

  }
  
 

  //{sessionid,roomid,turn,board,position,userid,status,connect1}
  async function saveMessage(id,player1_turn,player2_turn,board,player1_position,player2_position,player1_userid,player2_userid,player1_status,player2_status,player1_quit,player2_quit,player1_fakedisconnect,player2_fakedisconnect,player1_realdisconnect,player2_realdisconnect)
  {
    
   await  client.hSet(`room:${id}`,"player1_turn",JSON.stringify(player1_turn))
   await  client.hSet(`room:${id}`,"player2_turn",JSON.stringify(player2_turn))
   await  client.hSet(`room:${id}`,"board",JSON.stringify(board))
   await  client.hSet(`room:${id}`,"player1_position",JSON.stringify(player1_position),)
   await  client.hSet(`room:${id}`,"player2_position",JSON.stringify(player2_position))
   await  client.hSet(`room:${id}`,"player1_userid",player1_userid)
   await  client.hSet(`room:${id}`,"player2_userid",player2_userid)
   await  client.hSet(`room:${id}`,"player1_status",player1_status)
   await  client.hSet(`room:${id}`,"player2_status",player2_status)
   await  client.hSet(`room:${id}`,"player1_quit",player1_quit)
   await  client.hSet(`room:${id}`,"player2_quit",player2_quit)
   await  client.hSet(`room:${id}`,"player1_fakedisconnect",player1_fakedisconnect)
   await  client.hSet(`room:${id}`,"player2_fakedisconnect",player2_fakedisconnect)
   await  client.hSet(`room:${id}`,"player1_realdisconnect",player1_realdisconnect)
   await  client.hSet(`room:${id}`,"player2_realdisconnect",player2_realdisconnect)

  }
 //roomid,player1_turn,player2_turn,board,player1_postion,player2_position,player1_userid,
 //player2_userid,"player1_status","player2_status"

  async function findRoomplayer(id)
  {
    
    return await client.hGetAll(`room:${id}`);
  }

module.exports={findRoomplayer,deleteSessionMsg,updateMessageplayer1,updateMessageplayer2,saveMessage,updatequitplayer,updaterealdisconnectplayer1,updaterealdisconnectplayer2,updatefakedisconnectplayer1,updatefakedisconnectplayer2};





  // client.multi().hSet(`roomid:${roomid}`,"player1_turn","true").expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"player2_turn","false").expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"roomid",roomid).expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"board",JSON.stringify(board)).expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"player1_position",JSON.stringify(player1_position),).expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"player2_position",JSON.stringify(player2_position)).expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"player1_userid",JSON.stringify(player1_userid)).expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"player2_userid",JSON.stringify(player2_userid)).expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"player1_status",player1_status).expire(`roomid:${roomid}`,TTL).exec()
  //    client.multi().hSet(`roomid:${roomid}`,"player2_status",player2_status).expire(`roomid:${roomid}`,TTL).exec()