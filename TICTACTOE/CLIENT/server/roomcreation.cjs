  const crypto = require("crypto");
  const {findSession,updateSession,saveSession,deleteSession} = require('./sessionstore.cjs')
  const {findRoomplayer,deleteSessionMsg,updateMessageplayer1,updateMessageplayer2,saveMessage}=require('./messagestore.cjs')
  const {messagestore} = require('./messagestore.cjs')
  const rooms = new Map();
  const randomID = () => crypto.randomBytes(2).toString("hex");


 
  function createRoom(username)
  {
    console.log(`create room`);
    const roomid = randomID();
    const room={players:[],roomid:roomid,player1:username};
    rooms[roomid]=room;
    return room;
  }

  
  function findAvailableRoom(opponentname,socket) {
    console.log(`available room`);
    for (const roomId in rooms) {
      const room = rooms[roomId];
      console.log(`searching room ${roomId}`)
      if (room.players.length == 1 && room.players[0].connected) {
        rooms[roomId].player2=opponentname;
        rooms[roomId].players.push(socket);
        clearTimeout(rooms[roomId].timeoutId);
        console.log(`free room ${roomId}`)
        return room;
      }
    }
    return null;
  }
  
  function getRoomId(room) {
    console.log(`get room`);
    for (const roomId in rooms) {
      if (rooms[roomId] === room) {
        return roomId;
      }
    }
    return null;
  }
  
  
  function startGame(room) {

    
    console.log('Starting game...');
    clearTimeout(room.timeoutId);
    
    const sessionid1 = randomID();
    const sessionid2 = randomID();
    const userid1 = randomID();
    const userid2 = randomID();
    const player1 = room.players[0];
    const player2 = room.players[1];
    const roomid  = room.roomid;
    const username1  = room.player1; //pass usernames to startgame
    const username2  = room.player2;
    console.log(`player1 is ${player1}  username1 is ${username1} player2 is ${player2}`)
    player1.join(userid1);
    player2.join(userid2);
    player1.join(roomid);
    player2.join(roomid);

    console.log(`sessionid1 is ${sessionid1} sessionid2 is ${sessionid2}`);
    //all are socket1s
     //turn,usym,osym,uscore,board,position,roomid
    const session1 ={
      username:username1,
      opponentname:username2,
      userid:userid1,
      sessionid:sessionid1,
      roomid:roomid,
      board:['','','','','','','','','',''],
      turn:true,
      usym:'X',
      osym:'O',
      position:[0,0,0,0,0,0,0,0,0],
      status:0,
      connected:true
    };

    const session2 ={
      opponentname:username1,
      username:username2,
      userid:userid2,
      oppuserid:userid1,
      sessionid:sessionid2,
      roomid:roomid,
      board:['','','','','','','','','',''],
      turn:false,
      usym:'O',
      osym:'X',
      position:[0,0,0,0,0,0,0,0,0],
      status:0,
      connected:true
    };
    // send session to players
    if(player1.connected && player2.connected){

    player1.emit('session',session1);
    player2.emit('session',session2);
    saveSession(sessionid1,userid1,session1.username,roomid,session1.opponentname,'X','O',true);
    saveSession(sessionid2,userid2,session2.username,roomid,session2.opponentname,'O','X',true);

    //player1_turn,roomid,board,player1_postion,player1_userid,
  //player1_status
    saveMessage(roomid,true,false,['','','','','','','','',''],[0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,0,0,0],userid1,userid2,0,0,0,0,0,0,0,0);

    }
  // logic to cover quit status of the player if he left while match making
    else if(!player1.connected){
      console.log('Player 1 disconnected');
      player1.disconnect(true);
      player2.disconnect(true);
      delete rooms[getRoomId(room)];
    }
  
    else if(!player2.connected){
      console.log('Player 2 disconnected');
      //player1.emit('disconnect');
      player2.disconnect(true);
      player1.disconnect(true);
      delete rooms[getRoomId(room)];
    }
  }

   function handleConnection(username,socket) {

    console.log('looking room for player');
    //const username=socket.auth.username;
    let room = findAvailableRoom(username,socket);
    if (!room) {
      room = createRoom(username);
      room.players.push(socket);
      room.timeoutId = setTimeout(() => {
      console.log(`stuck in timeout`)
      if (room.players.length < 2) {
        console.log('Timeout reached.');
        const socket1=room.players[0];
        socket1.disconnect(true);
        console.log(`disconnected`);
        delete rooms[getRoomId(room)];
      }
    }, 20000);
  }
    
    const player1=room.players[0]; const player2=socket;
    console.log(room.players.length);
    if (room.players.length == 2 && player1.connected && player2.connected) {
      console.log(`start game`);
      startGame(room);
      delete rooms[getRoomId(room)];
    }
    
  }

  function deleteRedis(sessionid,roomid)
  {
    deleteSession(sessionid);
    deleteSessionMsg(roomid);
  }

  function updatesession(sessionid,connected)
  {
    updateSession(sessionid,connected)
  }
  //still has to work on this
 async function checkSession(sessionid1,roomid1,socket)
  {
  
   

  let {userid,username,roomid,opponentname,usym,osym,connected}=await findSession(sessionid1)
   
   console.log(`check session userid is ${userid}  username is ${username} opponentname is ${opponentname} session:${sessionid1}`);
   //let connected=false;
    if(userid)
    {
      connected=true;
      console.log(`user is still connected ${connected}`);
 
      let {player1_turn,player2_turn,board,player1_position,player2_position,player1_userid,player2_userid,player1_status,player2_status,player1_quit,player2_quit,player1_fakedisconnect,player2_fakedisconnect,player1_realdisconnect,player2_realdisconnect}=await findRoomplayer(roomid1);
     
  console.log(`player1_turn ${player1_turn} player2_turn ${player2_turn},board ${board}, ${player1_position},${player2_position},${player1_userid},${player2_userid},${player1_status},${player2_status},${player1_quit},${player2_quit},${player1_fakedisconnect},${player2_fakedisconnect},${player1_realdisconnect},${player2_realdisconnect}`)
      if(usym==='X'){
        console.log(`player user id ${player1_userid}`)

        if(!player1_userid && !player1_turn)
        {
          deleteRedis(sessionid1,roomid1);
          console.log('check room failed')
          socket.emit('session',{
          sessionid:0
          });
          return false;
        }

      
        player1_turn=JSON.parse(player1_turn),board=JSON.parse(board),player1_position=JSON.parse(player1_position),player1_status=parseInt(player1_status),player1_quit=parseInt(player1_quit),player2_fakedisconnect=parseInt(player2_fakedisconnect),player2_realdisconnect=parseInt(player2_realdisconnect);
        console.log(`board is ${board}`)
        console.log(board);

        socket.emit('session',{
          username:username,
        opponentname:opponentname,
        userid:userid,
        sessionid:sessionid1,
        roomid:roomid,
        turn:player1_turn,
        board:board,
        usym:usym,
        osym:osym,
        position:player1_position,
        status:player1_status,
        connected:true,
        quit:player1_quit,
        fakeoppdisconnect:player2_fakedisconnect,
        realoppdisconnect:player2_realdisconnect
        })
        return true;
      }
      else if(usym==='O')
      {
        console.log(`player user id ${player2_userid}`)
        if(!player2_userid &&  !player1_turn)
          {
            deleteRedis(sessionid1,roomid1);
            console.log('check room failed')
            socket.emit('session',{
            sessionid:0
            });
            return false;
          }
                      
player2_turn=JSON.parse(player2_turn),board=JSON.parse(board),player2_position=JSON.parse(player2_position),player2_status=parseInt(player2_status),player2_quit=parseInt(player2_quit),player1_fakedisconnect=parseInt(player1_fakedisconnect),player1_realdisconnect=parseInt(player1_realdisconnect);
          console.log(`board is ${board}`)
          console.log(board);

        
          socket.emit('session',{
            username:username,
          opponentname:opponentname,
          userid:userid,
          sessionid:sessionid1,
          roomid:roomid,
          turn:player2_turn,
          board:board,
          usym:usym,
          osym:osym,
          position:player2_position,
          status:player2_status,
          connected:true,
          quit:player2_quit,
          fakeoppdisconnect:player1_fakedisconnect,
          realoppdisconnect:player1_realdisconnect
          })
          return true;
      }
      

      
    }
    console.log(`checksession faileddd sorry user`)
    socket.emit('session',{
      sessionid:0
    });
    return false;
  }
  // { userid,username,roomid,opponentname,usym,osym,connected: connected === "true" }
  //{turn,roomid,board,position,status,cellid,oppncellid}

module.exports={handleConnection,checkSession,deleteRedis,updatesession};
  