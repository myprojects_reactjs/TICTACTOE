
//import {client} from './redisconnection.cjs';
// import { createRequire } from 'module';
// const require = createRequire(import.meta.url);
// const {client1}=require('./redisconnection.cjs')
const SESSION_TTL = 8 * 60;


const redis = require('redis');
 const client = redis.createClient({
    socket: {
        host: '127.0.0.1',
        port: '6379'
    },
  });
  client.connect();


 
    async function findSession(id) {
      
        return await client
          .hGetAll(`session:${id}`)
      }

    async function deleteSession(id)
    {
      console.log(`deleting session:${id}`)
      await client.hDel(`session:${id}`,
      "userid")
      await client.hDel(`session:${id}`,
      "username")
      await client.hDel(`session:${id}`,
      "roomid")
      await client.hDel(`session:${id}`,
      "opponentname")
      await client.hDel(`session:${id}`,
      "usym")
      await client.hDel(`session:${id}`,
      "osym")
      await client.hDel(`session:${id}`,
      "connected");
      

      
      
    }
    
    async function updateSession(id,connected)
    {
        await client.hSet(`session:${id}`,"connected",JSON.stringify(connected));
    }
   // { userID, username, connected,socket,roomid,opponentname }
    async function saveSession(id,userid,username,roomid,opponentname,usym,osym,connected) {

      console.log(`userid is ${userid} username is ${username} usym is ${usym} session:${id}`)
      await client.hSet(`session:${id}`,"userid",userid)
      await client.hSet(`session:${id}`,'username',username)
      await client.hSet(`session:${id}`,'roomid',roomid)
      await client.hSet(`session:${id}`,'opponentname',opponentname)
      await client.hSet(`session:${id}`,'usym',usym)
      await client.hSet(`session:${id}`,'osym',osym)
      await client.hSet(`session:${id}`,'connected',"true")
           
      
      }
      




module.exports={findSession,updateSession,saveSession,deleteSession};
