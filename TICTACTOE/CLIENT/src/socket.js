

import { io } from "socket.io-client";
const URL1 = "http://localhost:3000";
const URL2= "http://localhost:3001";


export const socket1 = io(URL1,{
    autoConnect:false,
    reconnection:false,
    reconnectionAttempts:5,
    reconnectionDelayMax:5000,
    reconnectionDelay:1000,
    timeout:3000
   
  });
 export const socket2 = io(URL2,{
    autoConnect:false,
    reconnection:false,
    reconnectionAttempts:5,
    reconnectionDelayMax:5000,
    reconnectionDelay:1000,
    timeout:3000
  });

