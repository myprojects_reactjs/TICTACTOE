//subbusaiayyappa
import React,{ useCallback, useEffect, useState} from 'react';
import './index.css';
import {socket1,socket2} from './socket.js';


export function Game(props)
{
 

    // let win=props.status.win;
    // let draw=props.status.draw;
    // let loss=props.status.loss;
   // let fakedisconnect=props.status.fake_oppdisconnect;
    //let realoppdisconnect=props.status.real_oppdisconnect;
     //let [quit,setQuit]=useState(props.status.quit);
    // let [timer,setTimer] = useState(15);
    // let [turntimer,setTurntimer] = useState(15);
    //its time for using usecallback
    
    // let [status,setStatus]=useState({quit:props.quit,status1:props.status
    // ,realdisconnect:props.real_oppdisconnect,fakedisconnect:props.fake_oppdisconnect})
    let [turn,setTurn]=useState(props.turn);
    let [quit,setQuit]=useState(props.quit);
    let [status,setStatus]=useState(props.status);
    let [fakedisconnect,setFakedisconnect]=useState(props.fake_oppdisconnect);
    let [realdisconnect,setRealdisconnect]=useState(props.real_oppdisconnect);

        let userid=props.userid
        let sessionid=props.sessionid
        let username=props.username
        let oppname=props.oppname
        let roomid =props.roomid;
        let usym=props.usym;
        let  osym=props.osym;
   
//    let [gd,setgd]=useState({
        
//     turn:props.gd.turn  
    
//   });

//   let [status,setStatus]=useState({
//     status:props.status.status,
//     quit:props.status.quit,
//     fake_oppdisconnect:props.status.fake_oppdisconnect,
//     real_oppdisconnect:props.status.real_oppdisconnect
//   })


 
//  const handleClick=useCallback((e)=>{
//         e.preventDefault();
//         console.log(`user not able to handleClick logged id ${e.target.id} props turn is ${props.gd.turn} gd turn is ${gd.turn}`);
//         //let turn1=localStorage.getItem('turn')
//         if(props.gd.turn===true){
//         let id=e.target.id;
//         console.log(`user handleClick logged id ${id} gd turn is ${props.gd.turn}`);
//         props.handleOnboard(id,false,props.gd.usym);
//       }
      
//     },[props.gd.turn]);

const handleClick=useCallback((e)=>{
    e.stopPropagation()
    e.preventDefault();
    console.log(`user not able to handleClick logged id ${e.target.id} gd turn is ${turn}`);
    if(turn===true){
    let id=e.target.id;
    props.handleOnboard(id,false,usym);
    console.log(`user handleClick logged id ${id} gd turn is ${turn}`);
    
    setTurn(false);
  }
},[props.turn]);

    let board=props.board.map((item)=>{ 
        return <div key={item.id} id={item.id} className='cell' onClick={handleClick}>{item.val}</div>
        });
    useEffect(()=>{
    if(turn)
    board=props.board.map((item)=>{ 
        return <div key={item.id} id={item.id} className='cell' onClick={handleClick}>{item.val}</div>
        });
        else
        board=props.board.map((item)=>{ 
            return <div key={item.id} id={item.id} className='cell'>{item.val}</div>
        });
    },[turn]);

    


  
    useEffect(()=>{
        console.log(`game board logged`);
  
    
    socket2.emit('join',{
        userid:userid,
        roomid:roomid
    });

    socket2.on('turn',(obj)=>{
        const id=obj.cellid;
        //const turn=obj.turn;
        console.log('turnn loggedd')
        console.log(`opponent cellid is ${id} ${obj.turn}`);
       // setgd({turn:!gd.turn})
        setTurn(obj.turn);
        props.handleOnboard(id,true,osym);

        //onClick1=handleClick;
         //opponent cellid and board gets updated 
    });

    socket2.on('quit',()=>{
       
        //setStatus({...status,quit:1,realdisconnect:1});
        setQuit(1);
        setRealdisconnect(1);
        props.handlerealoppdisconnect(1);
         // a msg like opponent user intentionally quit please quit the game
    console.log(`socket 2 quit logged`);
   
    //socket1.emit('quit');


   //this is imp and code has to done
    })
    
    socket2.on('disconnect',(data)=>{
        console.log(`socket2 disconnect logged in ${data}`);
        
    })
    socket2.on('status',(gameinfo)=>{

        let id;
        console.log('status loggeddd');
        setStatus(gameinfo.status);
        //setStatus({...status,quit:1,status1:gameinfo.status});
        props.handlestatus(gameinfo.status)
        console.log(`your win status ${props.status} ${gameinfo.status}`)
        setQuit(1);
        if(gameinfo.status===2)
        {
            id=gameinfo.cellid;
            props.handleOnboard(id,false,osym);
        
        }
        else if(gameinfo.status===3)
        {
            if(gameinfo.cellid)
            {
            id=gameinfo.cellid;
            props.handleOnboard(id,false,osym);
            }

            //this has to be handled properly
        }
    });

    socket2.on('playerdisconnect',(data)=>{
        // write logic fr running timer
        if(data.msg==='player disconnected')
        {
         console.log(`logged handlefakeoppdisconnect ${data.msg}`)
       // setStatus({...status,fakedisconnect:1});
       setFakedisconnect(1);
        props.handlefakeoppdisconnect(1);
        }
        else if(data.msg==='player connected')
        {
            setFakedisconnect(0);
          console.log(data.msg);
         // setStatus({...status,fakedisconnect:0})
        props.handlefakeoppdisconnect(0);

        }
          })

    return(()=>{
        console.log(`socket2 connection unmount`);
        // socket2.destroy();
    });
    
    },[])

    
 

    function handleGamedisconnection()
  {
    
    console.log('ur game session expired')
    setQuit(0);
    setStatus(0);
    setTurn(false);
    setFakedisconnect(0);
    setRealdisconnect(0);
 
  }
   
    
    // function handleReplay(e) // replay logic
    // {
    //     // send a replay req to the opp and wait for his call, create a promise here
    //     //if yes
    //     //send game data to the server 

    //     e.preventDefault(); 
    //    // setBoard([null,null,null,null,null,null,null,null,null]);
    //     // setWin(0);
    //     // setDraw(0);
    //     // setLoss(0);
    //     //setPosition([0,0,0,0,0,0,0,0,0]);
    //     //setOppncellid(-1);
    //     //setCellid(-1);
    //     //setTurn(true);

    //     // write socket logic
    // }

    const handleQuit=(e)=> //quit logic
    {
        e.stopPropagation();
        e.preventDefault();
        console.log(`handlequit loggedd`)
        console.log(`quit logged`);
        socket2.emit('quitting',{

            sessionid:sessionid,
            roomid:roomid
        });
        socket1.emit('quit',{
            sessionid:sessionid,
            roomid:roomid
        });
        // {status.quit===1?<h2>Opponent Quit,please leave the room </h2>:null}
         handleGamedisconnection();
        props.handleDisconnection(); // this is parent comp 
        //socket1.disconnect(true);
    }
        return(
        <div>
        <h2>Heyy {username}! Hurray MatchMaked...!!Enjoy the Game</h2>
        <h2>Your opponent is {oppname}</h2>
       
        {status===1?<h2>you wonn</h2>:null}
        {status===2?<h2>you loose</h2>:null}
        {status===3?<h2>Game drawn!! Better luck next time</h2>:null}
        {realdisconnect===1?<h2>Opponent Quit,please leave the room </h2>:null}
        {fakedisconnect===1?<h2>your opponent disconnected,you will be kicked out if he wont join</h2>:<h2>opponent Onlinee</h2>}
        {' '}
        <div>
            {turn?<h4>your turn {turn}</h4>:<h4>opponent turn {turn}</h4>}
        </div>
        <div className='parent'>
        <div className='board'>
        {board}
        </div>
        {' '}
        <div>
        {quit===0?<button onClick={(e)=>{e.preventDefault()}}>QUIT</button>:<button onClick={handleQuit}>QUIT</button>}
        </div>
        </div>
        </div>
        );
    
}

