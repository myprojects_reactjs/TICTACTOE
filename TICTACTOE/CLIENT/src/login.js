import React,{useEffect, useState, useCallback} from 'react';
import './index.css';
import {Game} from './board.js'; //lazy load this
import {socket1,socket2} from './socket.js';


export function Login() //main component
{
  // localStorage.removeItem('username');
  // localStorage.removeItem('sessionid')
  // localStorage.removeItem('roomid')
  let [username,setUsername] = useState(localStorage.getItem('username'));
  let [sessionid,setSessionid] = useState(localStorage.getItem('sessionid'));
  let [roomid,setRoomid]=useState(localStorage.getItem('roomid')); //save it
  let [isroom,setIsroom] = useState(false);
  let [discload,setDiscload]=useState({disconnected:true,loading:false});
  let [turn,setTurn]=useState(localStorage.getItem('turn'));
  let [gd_,setGd_]=useState({
    userid:'',
    sessionid:'',
    username:'',
    oppname:'',
    roomid:'',
    usym:'',
    osym:'',
    connected:false,
    board:[{id:0,val:''},{id:1,val:''},{id:2,val:''},{id:3,val:''},{id:4,val:''}
    ,{id:5,val:''},{id:6,val:''},{id:7,val:''},{id:8,val:''}],
    position:[{id:0,val:0},{id:1,val:0},{id:2,val:0},{id:3,val:0},{id:4,val:0}
    ,{id:5,val:0},{id:6,val:0},{id:7,val:0},{id:8,val:0}],
    status:0,
    quit:0,
    fake_oppdisconnect:0,
    real_oppdisconnect:0
   })
  
  

  //const handleNoclick=useCallback((e)=>e.preventDefault(),[gd_.turn]);

  
  useEffect(()=>{
    console.log(`logged with empty effect`);
/** */
  //  console.log('quit is called')
  //   socket1.emit('quit',{
  //     sessionid:sessionid,
  //     roomid: roomid
  //   });
    if(sessionid)
    {
     // setGd_({...gd_,turn:localStorage.getItem('turn')})
      console.log(sessionid);
      console.log(`saved session connection logged inn`);
      console.log(`socket1 logged ${sessionid} ${username}`);
      if(!socket1.connected){
      socket1.auth={sessionid,username,roomid};
      socket1.connect(); // this should emit "session" handler
      }
    }
    socket1.on('hi',(data)=>{
      console.log(`hii logged ${data.msg}`)
    })

    socket1.on('disconnect',(data)=>{
      console.log(`disconnect data is ${data}`);
      setIsroom(false);
      setDiscload({disconnected:true,loading:false});
      handleDisconnection();
    //  if(data==='io server disconnect' || data==='io client disconnect'||data==='server namespace /disconnect'){
      if(data!=="transport close"){
      localStorage.removeItem('username');
      localStorage.removeItem('sessionid')
      localStorage.removeItem('roomid')
      localStorage.removeItem('turn')
      //localStorage.removeItem('turn')

      }

    });
// this disconnect must be emitted only after deleting and erasing traces of session in the redis
// for intentional quit immediate effect can take place
// for unusual disconnection wait for 15secs for player to joiin 

    socket1.on('forcequit',()=>
    {
      console.log('forcequit logged in')
      socket1.emit('quit',{sessionid:sessionid,roomid:roomid});
      setIsroom(false);
      setDiscload({disconnected:true,loading:false});
     // handleDisconnection();
      // localStorage.removeItem('username');
      // localStorage.removeItem('sessionid')
      // localStorage.removeItem('roomid')

    })
    
    socket1.on('session',(session1)=>{
      handleSession(session1);
    });
    
    socket1.on('sample',(data)=>
    {
      console.log(data.msg)
    })

    socket1.on("connect_error", (err) => {
      console.log('connect error');
      if (err.message === "invalid username") {
        
        console.log('invalid username');
       // root.render('please enter proper username');
      }
      else
      {
        console.log(`err is ${err}`);
      }
    })

    socket1.io.on("reconnect_attempt", (attempt) => {

      console.log(`fired on reconnect attempt ${attempt}`);
    });
    
    socket1.io.on("reconnect_failed", () => {
      console.log('reconnection failedd...might be server errorr');
    });


    return (()=>{
      
      console.log(`login comp unmount`);
      socket1.disconnect(true);
      socket2.disconnect(true);

      //socket2.disconnect(true);
    // this io client disconnect
    })

  },[]);



  function handleChange(e)
  {
     
      setUsername(e.target.value);
  }

  function handleSubmit(e)
  {
    e.preventDefault(); 
    console.log('submit logged in')
    socket1.auth={username};
    socket1.connect();
    setDiscload({disconnected:false,loading:true});
    //localStorage.setItem('discload',{disconnected:false,loading:true});
    
  }

  const handleSession=(session1)=>
  {

    console.log(`logged in handleSession`);

    if(session1 && !sessionid){

      //id,userid,username,roomid,opponentname,usym,osym,connected
    setDiscload({disconnected:false,loading:false});
    console.log(`logged in new session`);
    setTurn(session1.turn);
    setGd_({...gd_,
      sessionid:session1.sessionid,
      oppname:session1.opponentname,
      usym:session1.usym,
      osym:session1.osym,
      roomid:session1.roomid,
      connected:session1.connected,
      userid:session1.userid
    })
    // turn sets to true or flase for the first time 
    if(session1.usym==='X')
        {
          socket2.auth={sessionid:sessionid,roomid:roomid,username:username,player:'1'};
        }
        else
        {
          socket2.auth={sessionid:sessionid,roomid:roomid,username:username,player:'2'};
        }
        socket2.connect();
   
    localStorage.setItem('sessionid', session1.sessionid);
    localStorage.setItem('username', session1.username);
    localStorage.setItem('roomid',session1.roomid);
    localStorage.setItem('turn',session1.turn)
    setIsroom(true); 



    }

    else if(session1 && sessionid === session1.sessionid)
    {
      
      // continue game
      console.log(`logged in exisiting session`);
      console.log('authenticated user is hitting request');
     // localStorage.setItem('turn',session1.turn);
     if(session1.usym==='X')
     {
       socket2.auth={sessionid:sessionid,roomid:roomid,username:username,player:'1'};
     }
     else
     {
       socket2.auth={sessionid:sessionid,roomid:roomid,username:username,player:'2'};
     }
     socket2.connect();

     const pd=session1.position;
            console.log(`position is ${pd}`)
            let pd1=[...gd_.position];
            pd1=pd1.map((p)=>{
              p.val=pd[p.id]
              return {id:p.id,val:p.val};
            });
            const bd=session1.board;
            let gd=[...gd_.board];
            gd=gd.map((g)=>{
      
              g.val=bd[g.id]
              return {id:g.id,val:g.val};
      
            });
      setTurn(session1.turn);
      setGd_(
        {
          ...gd_,
          userid:session1.userid,
          oppname:session1.opponentname,
          usym:session1.usym,
          osym:session1.osym,
          connected:session1.connected,
          board:gd,
          position:pd1,
          status:session1.status,
          quit:session1.quit,
          fake_oppdisconnect:session1.fake_oppdisconnect,
          real_oppdisconnect:session1.real_oppdisconnect
        })
        console.log(`new turn arrived from checksession is ${session1.turn} ${turn}`);

       // localStorage.setItem('turn',session1.turn);
         // console.log(`board`)
        //console.log([...gd_.board]);
      //  console.log(session1.board);
      
      setDiscload({disconnected:false,loading:false});
      setIsroom(true);

    }

    else
    {
      console.log(`session is ${session1.sessionid}`);
      setDiscload({disconnected:true,loading:false});
      localStorage.removeItem('sessionid');
      localStorage.removeItem('username');
      localStorage.removeItem('roomid');
      localStorage.removeItem('turn');

      // socket1.emit('quit',{sessionid:sessionid,roomid:roomid});
      // this quit should emit disconnet on the serrver
     handleDisconnection();

    }
  }

         
  function handleOnboard(id,turn,sym)
  {
    localStorage.setItem('turn',turn)
    console.log(`id is ${id} turn is ${turn} sym is ${sym}`)
    let temp1=[...gd_.board];
    temp1[id].val=sym;
    if(!turn && sym===gd_.usym)
    {
      
    let temp=[...gd_.position];
    temp[id].val=1;
    setTurn(turn);
    setGd_({
      ...gd_,
      position:temp,
      board:temp1
    });
    const squares=temp1.map((t)=>t.val);
    const pos=temp.map((t)=>t.val);
    const userid=gd_.userid;
    const roomid=gd_.roomid;
   // console.log(`userid ${userid} roomid ${roomid} board is ${gd_.board} turn is ${turn}`)

    const data={
      userid:userid,
      roomid:roomid,
      cellid:id,
      board:squares,
      position:pos,
      usym:sym
  }
    socket2.emit('gamedata',data);
    }
    
    else
    {
      setTurn(turn);
    setGd_({
      ...gd_,
      board:temp1,
    
    });
   

  }
    
  //localStorage.setItem('gamedata',{...gd_});
  }

  // const handleClick=(e)=>{
  //   e.stopPropagation()
  //   e.preventDefault();
  //   console.log(`user not able to handleClick logged id ${e.target.id} gd turn is ${ gd_.turn}`);
  //   if(gd_.turn===true){
  //   let id=e.target.id;
   
  //   console.log(`user handleClick logged id ${id} gd turn is ${ gd_.turn}`);
  //   // setGd_({
  //   //   ...gd_,
  //   //   turn:false
  //   // })
  //   handleOnboard(id,false,gd_.usym);
  // }
  
//  }
//e.stopPropagation()
  function handlequit()
  {
    console.log(`logged handlequit`)
    setGd_({...gd_,quit:1});
  }
   
  function handlerealoppdisconnect(real_oppdisconnect)
  {
    console.log(`logged handlerealoppdisconnect`)

    setGd_({...gd_,quit:1,real_oppdisconnect:real_oppdisconnect});
  }
  function handlestatus(status)
  {
    console.log(`logged handlestatus`)
    setGd_({...gd_,status:status,quit:1});
  }
  function handlefakeoppdisconnect(fake_oppdisconnect)
  {
    //console.log(`logged handlefakeoppdisconnect`)
    setGd_({...gd_,fake_oppdisconnect:fake_oppdisconnect});

  }

  function handleDisconnection()
  {
   
    console.log('handledisconnection logged');
    setUsername('');
    setSessionid('');
    setRoomid('');
    setIsroom(false);
    setGd_({
      username:null,
      oppname:null,
      roomid:null,
      usym:'',
      osym:'',
      userid:'',
      connected:false,
      usym:'',
      osym:'',
      turn:false,
      board:[{id:0,val:''},{id:1,val:''},{id:2,val:''},{id:3,val:''},{id:4,val:''}
  ,{id:5,val:''},{id:6,val:''},{id:7,val:''},{id:8,val:''}],
  position:[{id:0,val:0},{id:1,val:0},{id:2,val:0},{id:3,val:0},{id:4,val:0}
  ,{id:5,val:0},{id:6,val:0},{id:7,val:0},{id:8,val:0}],
      status:0,
      fake_oppdisconnect:0,
      real_oppdisconnect:0
  
  });
  setTurn(false);
    socket2.disconnect(true);
    
    console.log(`login expired ${username} ${new Date().toLocaleTimeString()}`);
  }
 


   
      return(
      <div className='roomid'>
        <div>
         {discload.loading===true?<h2><Loading username={username}/></h2>:null}
         {discload.disconnected===true?<Form username={username} onSubmit={handleSubmit} onChange={handleChange} />:null}
        </div>
        <div>
          {isroom===true?<Game {...gd_} turn={turn} handlerealoppdisconnect={handlerealoppdisconnect} handlefakeoppdisconnect={handlefakeoppdisconnect} handlequit={handlequit} handlestatus={handlestatus} handleOnboard={handleOnboard} handleDisconnection={handleDisconnection}/>:null}
        </div>
      </div>
      );
    }
    

function Loading({username})
{
  const ele=<div>Loading Gamee for you ...{username}</div>;
  return ele;
}

function Form({username,onSubmit,onChange})
{
    
  const ele=(
  <>
  <form onSubmit={onSubmit}>
    <label>
    {!username?<h3>Invalid username</h3>:null}
    <h3>Enter your name</h3>
      <textarea value={username} className='text' onChange={onChange} />
    </label>
    <div>
    <input type="submit" value="Submit" />
    </div> 
  </form>
  </>);

  return ele;
    
}

