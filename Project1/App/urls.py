from django.urls import path
from . import views

urlpatterns = [
    path('requirements/', views.RequirementsView, name="RequirementsForm"),
    path('require', views.RequireView, name="RequireForm"),
]