from distutils.command.upload import upload
from turtle import title
from django.db import models

class Post(models.Model):
    organization = models.CharField(max_length=255)
    movie_budget = models.IntegerField()
    game_list = models.CharField(max_length=255)
    mode_of_game = models.CharField(max_length=255)
    email = models.EmailField(max_length=100)
    
    logo = models.CharField(max_length=255, default= "")
    spl_instructions= models.CharField(max_length=255)
