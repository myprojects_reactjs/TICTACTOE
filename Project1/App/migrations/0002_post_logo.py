# Generated by Django 4.0.2 on 2022-02-28 07:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='logo',
            field=models.CharField(default='', max_length=255),
        ),
    ]
