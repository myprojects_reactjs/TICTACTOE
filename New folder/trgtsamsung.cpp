#include<bits/stdc++.h>
using namespace std;
class base
{
    protected:
    int x;
    public:
    base():x{0}{}
};
class derived:protected base{
public:
void fun(){
    cout<<x<<"derived"<<" ";
}

};
class derived1:public derived
{
   public: 
void fun1(){
    cout<<x<<"derived1"<<" ";
}


};
int main()
{
    derived *d=new derived();
    d->fun();
    derived1 *d1=new derived1();
    d1->fun1();


    return 0;
}