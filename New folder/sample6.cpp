// in a given array find the largest increasing subsequence
//array=[10,22,9,33,21,50,41,60,80]
#include<bits/stdc++.h>
using namespace std;

int lis(vector<int>array1)
{
vector<int>array2(array1.begin(),array1.end());
sort(array2.begin(),array2.begin()+array2.size());
vector<vector<int>>vec;
for(int i=0;i<array1.size();i++)
{
    vec.push_back(vector<int>(array1.size()+1,0));
}
for(int i=0;i<array1.size();i++)
{
    for(int j=0;j<array1.size();j++)
    {cout<<vec[i][j]<<" ";}
    cout<<"\n";

}

for(int i=1;i<array1.size()+1;i++){
    for(int j=1;j<array1.size()+1;j++)
    {
        if(array1[i]==array2[j])
        {
            vec[i][j]=vec[i-1][j-1]+1;
        }
        else
        {
            vec[i][j]=max(vec[i-1][j],vec[i][j-1]);
        }
    }
}


}

