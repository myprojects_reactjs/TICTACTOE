#include<bits/stdc++.h>
using namespace std;

struct node
{
int x;
struct node* next;
public:
node():x{0},next{nullptr}{}

};
class parent{
public:
parent(){cout<<"hi parent";}
void fun(){
    cout<<"hi fun1";
}
};
class child1:public parent
{
public:
child1(){cout<<"hi child";}
void fun()
{
    cout<<"hi fun2";
}

};

class parent1{
    public:
    virtual void fun()=0;
};
class parent2{
public:
virtual void fun()=0;
};
class child2:public parent1,public parent2
{
    public:
void fun()
{
    cout<<"hi"<<endl;
}
};
int main()
{
    node l;
    child1 c1;
    c1.parent::fun();
    child2 c2;
    c2.fun();
    return 0;
}