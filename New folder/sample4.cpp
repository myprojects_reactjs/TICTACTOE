//array=[4,8,2,7,6]
#include<bits/stdc++.h>
using namespace std;
vector<int> next_greatest(vector<int>&array)
{

stack<pair<int,int>>s;
vector<int>next_great(array.size(),0);
s.push({array[0],0});
for(int i=1;i<array.size();i++){
    int el=array[i];
    while(s.top().first<el){
        int x=s.top().second;
        next_great[x]=el;
        s.pop();

    }
    s.push({el,i});

}

while(!s.empty()){
    next_great[s.top().second]=-1;
    s.pop();
}

return next_great;
}

int main()
{
    vector<int>array{4,8,2,7,6};
    vector<int>a=next_greatest(array);
    for(auto i:a)
    cout<<i<<" ";
    return 0;
}